<?php

require_once 'controller.php';


$action= $_GET['action'];

if($action == 'create') {
	$nom= $package->escapeField($_POST['nom']);

	$typedocumentdb->create($nom);

	$_SESSION['error']= 'Type de document ajouté avec succès';
	header('Location:../app.php?view=typedocument_create');
}



if($action == 'update') {
	$idtypedocument= $_POST['idtypedocument'];
	$nom= $package->escapeField($_POST['nom']);

	$typedocumentdb->update($idtypedocument, $nom);

	$_SESSION['error']= 'Type de document modifié avec succès';
	header('Location:../app.php?view=typedocument');
}




if($action == 'delete') {
	$idtypedocument= $_GET['p'];
	$typedocumentdb->delete($idtypedocument);

	$_SESSION['error']= 'Type de document supprimée avec succès';
	header('Location:../app.php?view=typedocument');
}


?>