<?php

session_start();

require_once '../model/TypedocumentDB.php';
require_once '../model/UserDB.php';
require_once '../model/DocumentDB.php';

require_once '../manage/Package.php';
require_once '../manage/Upload.php';
require_once '../manage/SwiftMailerClass.php';
// require_once '../manage/PJmailClass.php';



$typedocumentdb= new TypedocumentDB();
$userdb= new UserDB();
$documentdb= new DocumentDB();


$package= new Package();
$upload= new Upload();
$swiftmailer= new SwiftMailerClass();
//$pjmail= new PJmailClass();



?>