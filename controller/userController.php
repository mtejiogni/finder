<?php

require_once 'controller.php';

$prefix= 'user';
$width_limit= '100';
$height_limit= '100';
$path_folder= './ressources/user/';

$action= $_GET['action'];







if($action == 'createProfil') {
	$iddomaine= $_POST['iddomaine'];
	$nom= $package->escapeField($_POST['nom']);
	$telephone= $package->escapeField($_POST['telephone']);
	$email= $package->escapeField($_POST['email']);
	$adresse= $package->escapeField($_POST['adresse']);
	$login= $email;
	$password= $telephone;
	$role= 'Apprenant';
	$image= '';


	if($iddomaine == null || $iddomaine == '' || $iddomaine == 'choix') {
		$_SESSION['error']= 'Le domaine de formation est invalide';
		header('Location:../connexion.php');
	}
	else if($nom == null || $nom == '') {
		$_SESSION['error']= 'Le nom est invalide';
		header('Location:../connexion.php');
	}
	else if($telephone == null || $telephone == '') {
		$_SESSION['error']= 'Le numéro de téléphone est invalide';
		header('Location:../connexion.php');
	}
	else if($email == null || $email == '') {
		$_SESSION['error']= 'L\'adresse email est invalide';
		header('Location:../connexion.php');
	}
	else {
		$user= $userdb->readCompte($login, $password);

		if($user == false) {
			$userdb->create($iddomaine, $nom, $telephone, $email, $adresse, $login, $password, $role, $image);

			$_SESSION['error']= 'Votre compte à bien été crée. \nLes paramètres de connexion ont été envoyé à votre adresse mail';
			header('Location:../connexion.php');
		}
		else {
			$_SESSION['error']= 'Impossible de créer un tel compte changé vos paramètres s\'il vous plaît.';
			header('Location:../connexion.php');
		}
	}

}


















if($action == 'create') {
	$iddomaine= null;
	if($_POST['role'] == 'Apprenant') {
		$iddomaine= $_POST['iddomaine'];
	}

	$nom= $package->escapeField($_POST['nom']);
	$telephone= $package->escapeField($_POST['telephone']);
	$email= $package->escapeField($_POST['email']);
	$adresse= $package->escapeField($_POST['adresse']);
	$login= $package->escapeField($_POST['login']);
	$password= $package->escapeField($_POST['password']);
	$role= $_POST['role'];
	$image= '';

	$user= $userdb->readCompte($login, $password);

	if($user == false) {

		if(isset($_FILES['image']) == true && $_FILES['image']['size'] != 0) {
			$image= $upload->upload_image($_FILES['image'], $prefix, $width_limit, $height_limit, $path_folder);
		}

		$userdb->create($iddomaine, $nom, $telephone, $email, $adresse, $login, 
                            $password, $role, $image);

		$_SESSION['error']= 'Utilisateur ajouté avec succès';
		header('Location:../app.php?view=user_create');
	}
	else {
		$_SESSION['error']= 'Ce compte existe déjà';
		header('Location:../app.php?view=user_create');
	}
}








if($action == 'update') {
	$iduser= $_POST['iduser'];
	$user= $userdb->read($iduser);

	$iddomaine= null;
	if($_POST['role'] == 'Apprenant') {
		$iddomaine= $_POST['iddomaine'];
	}

	$nom= $package->escapeField($_POST['nom']);
	$telephone= $package->escapeField($_POST['telephone']);
	$email= $package->escapeField($_POST['email']);
	$adresse= $package->escapeField($_POST['adresse']);
	$login= $package->escapeField($_POST['login']);
	$password= $package->escapeField($_POST['password']);
	$role= $_POST['role'];
	$image= $user->image;

	$user= $userdb->readCompte($login, $password);

	if($user == false || ($user != false && $user->iduser == $iduser)) {

		if(isset($_FILES['image']) == true && $_FILES['image']['size'] != 0) {
			$image= $upload->upload_image($_FILES['image'], $prefix, $width_limit, $height_limit, $path_folder);

			unlink($path_folder . $user->image);
		}

		$userdb->update($iduser, $iddomaine, $nom, $telephone, $email, $adresse, $login, $password, $role, $image);

		$_SESSION['error']= 'Utilisateur modifié avec succès';
		header('Location:../app.php?view=user');
	}
	else {
		$_SESSION['error']= 'Ce compte existe déjà';
		header('Location:../app.php?view=user_update&p='. $iduser);
	}
}














if($action == 'updateProfil') {
	$iduser= $_POST['iduser'];
	$user= $userdb->read($iduser);

	$nom= $package->escapeField($_POST['nom']);
	$telephone= $package->escapeField($_POST['telephone']);
	$email= $package->escapeField($_POST['email']);
	$adresse= $package->escapeField($_POST['adresse']);
	$login= $package->escapeField($_POST['login']);
	$password= $package->escapeField($_POST['password']);
	$image= $user->image;

	$user= $userdb->readCompte($login, $password);

	if($user == false || ($user != false && $user->iduser == $iduser)) {

		if(isset($_FILES['image']) == true && $_FILES['image']['size'] != 0) {
			$image= $upload->upload_image($_FILES['image'], $prefix, $width_limit, $height_limit, $path_folder);

			unlink($path_folder . $user->image);
		}

		$userdb->updateProfil($iduser, $nom, $telephone, $email, $adresse, $login, $password, $image);

		$_SESSION['profil']= $userdb->read($iduser);
		$_SESSION['error']= 'Le compte a été modifié avec succès';
		header('Location:../app.php?view=profil&p='. $iduser);
	}
	else {
		$_SESSION['error']= 'Erreur lors de la modification changer vos paramètres s\'il vous plaît';
		header('Location:../app.php?view=profil&p='. $iduser);
	}
}
















if($action == 'delete') {
	$iduser= $_GET['p'];
	$user= $userdb->read($iduser);

	unlink($path_folder . $user->image);
	$userdb->delete($iduser);

	$_SESSION['error']= 'Utilisateur supprimé avec succès';
	header('Location:../app.php?view=user');
}


?>