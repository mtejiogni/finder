const sign_in_btn = document.querySelector("#sign-in-btn");
const sign_up_btn = document.querySelector("#sign-up-btn");
const container = document.querySelector(".container");

sign_up_btn.addEventListener('click', () => {
    container.classList.add("sign-up-mode");
})
sign_in_btn.addEventListener('click', () => {
    container.classList.remove("sign-up-mode");
})



document.querySelector("#form_inscription").onsubmit= function() {
	var submit= true;

	var nom= document.querySelector("#nom").value;
	var telephone= document.querySelector("#telephone").value;
	var email= document.querySelector("#email").value;
	var adresse= document.querySelector("#adresse").value;
	var iddomaine= document.querySelector("#iddomaine").value;

	var error= document.querySelector("#error_inscription i");

	if(telephone.length > 15 || isNaN(parseInt(telephone)) == true) {
		error.textContent= 'Numéro de téléphone invalide';
		submit= false;
	}
	else if(email.split('@').length != 2) {
		error.textContent= 'Email invalide';
		submit= false;
	}
	else if(iddomaine == 'choix') {
		error.textContent= 'Choix de la formation invalide';
		submit= false;
	}

	return submit;
}





