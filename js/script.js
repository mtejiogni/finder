
//Fonction qui permet d'effacer les caractères spéciaux
function deleteSpecialCharacter($selector) {
	$chaine= "`#§<>'\"&|![]{}=:\\?";

	$("body").on('keyup', $selector, function(evt) {
		if($chaine.indexOf(evt.key) != -1) {
			$(this).val($(this).val().replace(evt.key, ""));
		}
	});
}






/***************** Distance entre deux points geographique *******************/

function distance_map(lat1, lon1, lat2, lon2) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
        return 0;
    }
    else {
        var radlat1 = Math.PI * lat1/180;
        var radlat2 = Math.PI * lat2/180;
        var theta = lon1-lon2;
        var radtheta = Math.PI * theta/180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180/Math.PI;
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return dist;
    }
}


/***************** Fin Distance entre deux points geographique *******************/




























/***************** Chargement des valeurs par defaut sur les formulaires **************/

if(document.querySelector('input[type="search"]') != null) {
	var searchs= document.querySelectorAll('input[type="search"]');
	
	for(var i= 0; i<searchs.length; i++) {
		searchs[i].value= '';
	}
}

if(document.querySelector('input[type="file"]') != null) {
	var files= document.querySelectorAll('input[type="file"]');
	
	for(var i= 0; i<files.length; i++) {
		files[i].value= '';
	}
}


/*
if($('input[type="date"]') != null) {
	$('input[type="date"]').datepicker($.datepicker.regional["fr"]);
}

if($('.input_date') != null) {
	$('.input_date').datepicker($.datepicker.regional["fr"]);
}
*/

/***************** Fin Chargement des valeurs par defaut sur les formulaires **************/

































/***************** Verification des champs **************/

//--------> Input number
if($('input[type="number"]') != null) {
	$('body').on('keyup', 'input[type="number"]', function() {
		$valeur= parseInt($(this).val());

		if(isNaN($valeur) == true) {
			$(this).val('0');
		} 
	});
}

/***************** Fin Verification des champs **************/






































/***************** Affichages des mots de passe **************/

$('form .show-password').css('display', 'none');

$('body').on('click', 'form .icon-password', function() {
	$hide_elt= $(this).parent().find('.hide-password');
	$show_elt= $(this).parent().find('.show-password');
	$input= $(this).parent().find('input');

	if($hide_elt.css('display') != 'none') {
		$hide_elt.css('display', 'none');
		$show_elt.css('display', '');
		$input.attr('type', 'text');
	}
	else {
		$hide_elt.css('display', '');
		$show_elt.css('display', 'none');
		$input.attr('type', 'password');
	}
});

/***************** Fin Affichages des mots de passe **************/



































/************************** Module de recherche *****************************/

if(document.querySelector("#search") != null) {
	
	document.querySelector("#search").onkeyup= function() {
		var search= this.value.toLowerCase();
		var elements= document.querySelectorAll(".element");


		for(var i= 0; i<elements.length; i++) {
			var datas= elements[i].querySelectorAll(".data"); 
			var chaine= '';

			for(var j= 0; j<datas.length; j++) {
				chaine= chaine + datas[j].textContent + ' ';
			}

			chaine= chaine.toLowerCase();

			if(chaine.indexOf(search) == -1) {
				elements[i].style= "display: none;";
			}
			else {
				elements[i].style= "display: ;";
			}
		}
	}
}



/************************** Fin Module de recherche *****************************/












































/************************** Geolocalisation ************************/

/*------------------------------ API LEAFLET ----------------------*/

function map_load_leaflet(mapid, maplocate) {
	animateOn();

	// Initialisation de la carte

	var map = L.map(mapid).fitWorld();
	


	// Choix système de rendu de carte
	// id: 'mapbox.streets' ou id: 'mapbox.satellite'

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; ' +
					 '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					 '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
					 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1IjoibWFyYzEyMzQ1Njc4OSIsImEiOiJjanp2MHR0MzQwMWI4M2NueG9zaXhmbTNvIn0.jG9sa00SY9UH7zWclUs-KA'
	}).addTo(map);


	map.locate({setView: true, maxZoom: 16});
	

	// Gestion des objets

	var layerGroup = L.layerGroup().addTo(map);





	// Gestion des evenements
	
	map.on('locationfound', function(e){

		var marker = L.marker(e.latlng).addTo(layerGroup);
		marker.bindPopup("Vous êtes ici").openPopup();


		var circle = L.circle(e.latlng, {
			color: 'blue',
			fillColor: '',
			fillOpacity: 0.3,
			radius: 40
		}).addTo(layerGroup);


		document.querySelector(maplocate).value= e.latlng.lat + ',' + e.latlng.lng;
		animateOff();

	});




	map.on('locationerror', function(e){
		alert(e.message);
		animateOff();
	});




	map.on('click', function(e){
		layerGroup.clearLayers();

		var marker = L.marker(e.latlng).addTo(layerGroup);
		marker.bindPopup("Choix du point de livraison").openPopup();
		

		var circle = L.circle(e.latlng, {
			color: 'blue',
			fillColor: '',
			fillOpacity: 0.3,
			radius: 40
		}).addTo(layerGroup);


		document.querySelector(maplocate).value= e.latlng.lat + ',' + e.latlng.lng;

	});

}














function map_load_view_leaflet(mapid, maplocate) {
	animateOn();

	// Initialisation de la carte

	var latitude= document.querySelector(maplocate).value.split(',')[0];
	var longitude= document.querySelector(maplocate).value.split(',')[1];
	var map = L.map(mapid).setView([latitude, longitude], 16);
	


	// Choix système de rendu de carte
	// id: 'mapbox.streets' ou id: 'mapbox.satellite'

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; ' +
					 '<a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
					 '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
					 'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1IjoibWFyYzEyMzQ1Njc4OSIsImEiOiJjanp2MHR0MzQwMWI4M2NueG9zaXhmbTNvIn0.jG9sa00SY9UH7zWclUs-KA'
	}).addTo(map);





	// Gestion des objets

	var layerGroup = L.layerGroup().addTo(map);


	var marker = L.marker([latitude, longitude]).addTo(layerGroup);
	marker.bindPopup("Position actuelle").openPopup();


	var circle = L.circle([latitude, longitude], {
		color: 'blue',
		fillColor: '',
		fillOpacity: 0.3,
		radius: 40
	}).addTo(layerGroup);

	animateOff();




	// Gestion des evenements

	map.on('click', function(e){
		layerGroup.clearLayers();

		var marker = L.marker(e.latlng).addTo(layerGroup);
		marker.bindPopup("Choix du point de livraison").openPopup();
		

		var circle = L.circle(e.latlng, {
			color: 'blue',
			fillColor: '',
			fillOpacity: 0.3,
			radius: 40
		}).addTo(layerGroup);


		document.querySelector(maplocate).value= e.latlng.lat + ',' + e.latlng.lng;

	});

}

/*------------------------------ FIN API LEAFLET ----------------------*/



























/*---------------------------- API GOOGLE MAPS ----------------------*/

function map_load_gmap(mapid, maplocate) {
	animateOn();

	var latitude= 4.077626280451048;
	var longitude= 9.79712247848;
	var map;
	var infoWindow;
	var marker;
	var circle;

	map = new google.maps.Map(document.getElementById(mapid), {
	  center: new google.maps.LatLng(latitude, longitude),
	  zoom: 8,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	});


	if (navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition(function(position) {
        	var pos = {
          		lat: position.coords.latitude,
          		lng: position.coords.longitude
        	};

        	marker = new google.maps.Marker({
        		map: map, 
        		position: pos
        	});

        	circle = new google.maps.Circle({
			  	center: marker.getPosition(),
			  	strokeColor: 'rgba(255, 0, 0, 0.8)',
			  	strokeOpacity: 0.8,
            	strokeWeight: 2,
				fillColor: 'rgba(255, 0, 0, 0.5)',
				fillOpacity: 0.3,
			  	radius: 60,
			  	map: map
			});

        	infoWindow= new google.maps.InfoWindow;
        	infoWindow.setContent('<b>Vous êtes ici</b>');
        	infoWindow.open(map, marker);

        	marker.addListener('click', function() {
				map.setZoom(16);
		    	infoWindow.open(map, marker);
		    	map.setCenter(marker.getPosition());
		    });

        	map.setCenter(pos);

        	document.querySelector(maplocate).value= pos.lat + ',' + pos.lng;
        	animateOff();
      	}, function() {
      		animateOff();
        	handleLocationError(true, infoWindow, map.getCenter());
      	});
    } 
    else {
    	animateOff();
      	// Browser doesn't support Geolocation
      	handleLocationError(false, infoWindow, map.getCenter());
    }


    map.addListener('click', function(e) {
    	var pos = {
      		lat: e.latLng.lat(),
      		lng: e.latLng.lng()
    	};

    	marker.setPosition(pos);
    	circle.setCenter(pos);

    	infoWindow.setContent('Choix du point de livraison');
    	infoWindow.open(map, marker);

    	map.setCenter(pos);
    	document.querySelector(maplocate).value= pos.lat + ',' + pos.lng;
    });

}













function map_load_view_gmap(mapid, maplocate) {
	animateOn();

	var latitude= parseFloat(document.querySelector(maplocate).value.split(',')[0]);
	var longitude= parseFloat(document.querySelector(maplocate).value.split(',')[1]);
	var map;
	var infoWindow;
	var marker;
	var circle;

	map = new google.maps.Map(document.getElementById(mapid), {
	  center: new google.maps.LatLng(latitude, longitude),
	  zoom: 16,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	});


	var pos = {
  		lat: latitude,
  		lng: longitude
	};

	marker = new google.maps.Marker({
		map: map, 
		position: pos
	});

	circle = new google.maps.Circle({
	  	center: marker.getPosition(),
	  	strokeColor: 'rgba(255, 0, 0, 0.8)',
	  	strokeOpacity: 0.8,
    	strokeWeight: 2,
		fillColor: 'rgba(255, 0, 0, 0.5)',
		fillOpacity: 0.3,
	  	radius: 40,
	  	map: map
	});

	infoWindow= new google.maps.InfoWindow;
	infoWindow.setContent('<b>Position actuelle</b>');
	infoWindow.open(map, marker);

	marker.addListener('click', function() {
    	infoWindow.open(map, marker);
    });

	map.setCenter(pos);

	animateOff();


	map.addListener('click', function(e) {
    	var pos = {
      		lat: e.latLng.lat(),
      		lng: e.latLng.lng()
    	};

    	marker.setPosition(pos);
    	circle.setCenter(pos);

    	infoWindow.setContent('Choix du point de livraison');
    	infoWindow.open(map, marker);

    	map.setCenter(pos);
    	document.querySelector(maplocate).value= pos.lat + ',' + pos.lng;
    });

}


/*---------------------------- FIN API GOOGLE MAPS ----------------------*/


















//--------> Verification du champ de geolocalisation

if(document.querySelector("#enreg") != null && 
	document.querySelector("#enreg #map") != null &&
	document.querySelector("#enreg #envoyer") != null) {

	document.querySelector("#enreg #envoyer").onclick= function() {
		var str= document.querySelector("#enreg #map").value;

		if(str == '') {
			alert('Bien vouloir renseigné les coordonnées géographiques');
		}
	}

}








//--------> Ouvrir la fenetre de geolocalisation

if(document.querySelector("#map-save") != null) {
	document.querySelector("#map-save").onclick= function() {
	    document.querySelector("#map-panel").style= "display: block;";
	    $("#map-id").remove();
	    $("#map-panel").append('<div id="map-id"></div>');

	    var action= document.querySelector("#enreg #form_enreg").getAttribute("action");

	    if(action.indexOf("?action=create") != -1) {
			map_load_gmap('map-id', '#map');
		}
		else {
			map_load_view_gmap('map-id', '#map');
		}
	    
	}
}







//--------> Fermer la fenetre de geolocalisation

if(document.querySelector("#map-close") != null) {
	document.querySelector("#map-close").onclick= function() {
	    document.querySelector("#map-panel").style= "display: none;";
	}
}


/************************ Fin Geolocalisation **********************/

















































/************************** Gestion de la navigation *****************************/

//--------> Fermer la fenetre d'enregistrement

if(document.querySelector("#enreg .icon") != null) {
	document.querySelector("#enreg .icon").onclick= function() {
	    document.querySelector("#enreg").style= "display: none;";
	}
}






//----> Ajouter

if(document.querySelector("#ajouter") != null) {
	document.querySelector("#ajouter").onclick= function() {
	    document.querySelector("#enreg").style= "display: block;";

	    var action= formulaire(document.querySelector("#enreg #form_enreg").getAttribute("action"));
	    document.querySelector("#enreg #form_enreg").setAttribute("action", action+"?action=create");
	}
}








//----> Menu

if(document.querySelector("#icon_menu") != null) {
	document.querySelector("#icon_menu").onclick= function() {
		if(document.querySelector("#menus").style.display == "none" ||
			document.querySelector("#menus").style.display == "") {
			document.querySelector("#menus").style.display= "block";
			document.querySelector('#menus').style.animation= 'slideInLeft 0.5s';
		}
		else {
			document.querySelector("#menus").style.display= "none";
		}
	}


	document.querySelector("#menus .menu-close").onclick= function() {
		document.querySelector("#menus").style.display= "none";
	}
}


/************************** Fin Gestion de la navigation *****************************/








































