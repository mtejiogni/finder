<?php

session_start();


/****************** Gestion du Profil ***********************/

if(isset($_SESSION['profil']) != true) {
  header('Location:connexion.php');
}

/**************** Fin Gestion du Profil *********************/




/******************** Gestion du Model **********************/

require_once 'model/UserDB.php';
require_once 'model/DomaineDB.php';
require_once 'model/CategorieDB.php';
require_once 'model/QuestionDB.php';
require_once 'model/PropositionDB.php';
require_once 'model/CompositionDB.php';
require_once 'model/EvaluationDB.php';
require_once 'model/SchoolDB.php';

require_once 'manage/Package.php';
require_once 'manage/Upload.php';

$userdb= new UserDB();
$domainedb= new DomaineDB();
$categoriedb= new CategorieDB();
$questiondb= new QuestionDB();
$propositiondb= new PropositionDB();
$compositiondb= new CompositionDB();
$evaluationdb= new EvaluationDB();
$schooldb= new SchoolDB();

$package= new Package();
$upload= new Upload();

/**************** Fin Gestion du Model ********************/






/****************** Gestion des Erreurs *******************/

if(isset($_SESSION['error']) == true) {
  echo '<script type="text/javascript">';
  echo 'alert("'. $_SESSION['error'] .'");';
  echo '</script>';
  unset($_SESSION['error']);
}

/**************** Fin Gestion des Erreurs *****************/






/****************** Gestion de la vue *******************/

$view= null;
if(isset($_GET['view']) == true) {
  $view= $_GET['view'];
}

/**************** Fin Gestion de la vue *****************/







/****************** Gestion des ressources *******************/

$res_user= './controller/ressources/user/';
$res_question= './controller/ressources/question/';
$res_proposition= './controller/ressources/proposition/';
$res_composition= './controller/ressources/composition/';

/**************** Fin Gestion des ressources *****************/















/****************** Quelques elements du profil *******************/

$compositions_profil= array();

if($_SESSION['profil']->role == 'Apprenant') {
  $categories= $categoriedb->readIddomaine($_SESSION['profil']->iddomaine);

  if($categories != null && sizeof($categories) != 0) {
    foreach($categories as $categorie) {
      $compos= $compositiondb->readCategorieEtat($categorie->idcategorie, 'Activé');

      if($compos != null && sizeof($compos) != 0) {
        foreach($compos as $composition) {
          array_push($compositions_profil, $composition);
        }
      }
    }
  }
}
else {
  $compositions_profil= $compositiondb->readEtat('Activé');
}

/****************** Fin Quelques elements du profil *******************/



?>




<!DOCTYPE html>
<html lang="fr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Teach</title>


    <!-- ************** Gestion du CSS ************** -->
    <link rel="icon" type="image/x-icon" href="img/favicon.ico" />

    <link rel="stylesheet" href="assets/vendors/mdi/css/materialdesignicons.min.css" />
    <link rel="stylesheet" href="assets/vendors/flag-icon-css/css/flag-icon.min.css" />
    <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css" />
    <link rel="stylesheet" href="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css" />
    <link rel="stylesheet" href="assets/css/template.css" />


    <!-- ++++++++++++++++++++++++++++ -->
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="css/fontawesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.min.css" />

    <!-- ++++ Generer des alerts personnalisees -->
    <link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css" />

    <!-- ++++ Input file personnalise avec Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/fileinput/fileinput.css" />

    <!-- ++++ Select personnalise -->
    <link rel="stylesheet" type="text/css" href="css/chosen/prism.css" />
    <link rel="stylesheet" type="text/css" href="css/chosen/chosen.css" />
    

    <link rel="stylesheet" type="text/css" href="css/fonts.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/color.css" />

    <!-- ++++++++++++++++++++++++++++ -->












    <!--***************** Gestion du JavaScript ***************-->

    <!-- ++++++++++++++++++++++++++++ -->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.datepicker-fr.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    
    <!-- ++++ Generer des alerts personnalisees -->
    <script type="text/javascript" src="js/sweetalert2.all.min.js"></script>
    
    <!-- ++++ Dessiner des graphes -->
    <script type="text/javascript" src="js/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="js/highcharts/exporting.js"></script>
    <script type="text/javascript" src="js/highcharts/export-data.js"></script>
    
    <!-- ++++ Input file personnalise avec Bootstrap -->
    <script type="text/javascript" src="js/fileinput/plugins/piexif.min.js"></script>
    <script type="text/javascript" src="js/fileinput/plugins/purify.min.js"></script>
    <script type="text/javascript" src="js/fileinput/plugins/sortable.min.js"></script>
    <script type="text/javascript" src="js/fileinput/fileinput.min.js"></script>
    <script type="text/javascript" src="js/fileinput/locales/fr.js"></script>
    <script type="text/javascript" src="js/fileinput/themes/fas/theme.min.js"></script>

    <!-- ++++ Select personnalise -->
    <script type="text/javascript" src="js/chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/chosen/prism.js"></script>
    <script type="text/javascript" src="js/chosen/init.js"></script>


    <script type="text/javascript" src="js/package.js"></script>

    <!-- ++++++++++++++++++++++++++++ -->


    <!-- plugins:js -->
    <script src="assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="assets/vendors/chart.js/Chart.min.js"></script>
    <script src="assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="assets/vendors/flot/jquery.flot.js"></script>
    <script src="assets/vendors/flot/jquery.flot.resize.js"></script>
    <script src="assets/vendors/flot/jquery.flot.categories.js"></script>
    <script src="assets/vendors/flot/jquery.flot.fillbetween.js"></script>
    <script src="assets/vendors/flot/jquery.flot.stack.js"></script>
    <script src="assets/vendors/flot/jquery.flot.pie.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="assets/js/off-canvas.js"></script>
    <script src="assets/js/hoverable-collapse.js"></script>
    <script src="assets/js/misc.js"></script>
    <!-- endinject -->







    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <script type="text/javascript" src="js/html5shiv-printshiv.min.js"></script>
        <script type="text/javascript" src="js/respond.min.js"></script>
    <![endif]-->
    
  </head>
  <body>
    <div class="container-scroller">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a class="sidebar-brand brand-logo logo_app" href="">
            Teach
            <!-- <img src="assets/images/logo.svg" alt="logo" /> -->
          </a>
          <a class="sidebar-brand brand-logo-mini pl-4 pt-3 logo_min_app" href="">
            Teach
            <!-- <img src="assets/images/logo-mini.svg" alt="logo" /> -->
          </a>
        </div>






        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">

                <!-- Chargement image de profil -->

                <?php if($_SESSION['profil']->image == '' ||
                      $_SESSION['profil']->image == null) { ?>

                <img src="img/avatar.png" alt="profil" class="img-responsive" />

                <?php } else { ?>

                <img src="<?php echo $res_user.$_SESSION['profil']->image ?>" alt="profil" class="img-responsive" />

                <?php } ?>

                <!-- Fin Chargement image de profil -->


                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
              </div>
              <div class="nav-profile-text d-flex flex-column pr-3">
                <span class="font-weight-medium mb-2">
                  <?php echo explode(' ', $_SESSION['profil']->nom)[0]; ?>
                </span>
                <span class="font-weight-normal" style="font-size:7pt;">
                  <?php echo $_SESSION['profil']->role ?>
                </span>
              </div>
              <span class="badge badge-info text-white ml-3 rounded">
                <?php echo sizeof($compositions_profil); ?>
              </span>
            </a>
          </li>


          <div class="text-center">
            <span class="btn btn-info btn-xs" style="border-radius: 100px;" onclick="document.location.href='<?php echo 'app.php?view=profil&p='. $_SESSION['profil']->iduser; ?>'">
              <i class="fas fa-user"></i>
            </span>

            <span class="btn btn-danger btn-xs" style="border-radius: 100px;" onclick="document.location.href='controller/deconnexionController.php'">
              <i class="fa fa-sign-out-alt"></i>
            </span>

            <br /><br />
          </div>


          <li class="nav-item">
            <a class="nav-link" href="app.php?view=start">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Tableau de Bord</span>
            </a>
          </li>





          <li class="nav-item">
            <a class="nav-link" href="app.php?view=mescompositions">
              <i class="mdi mdi-launch menu-icon"></i>
              <span class="menu-title">Démarrer une<br />composition</span>
            </a>
          </li>







          <?php if($_SESSION['profil']->role == "Super-Administrateur" ||
                  $_SESSION['profil']->role == "Administrateur") { ?>


          <li class="nav-item">
            <a class="nav-link" href="app.php?view=evaluation">
              <i class="mdi mdi-school menu-icon"></i>
              <span class="menu-title">Gestion des<br />évaluations</span>
            </a>
          </li>





          <li class="nav-item">
            <a class="nav-link" href="app.php?view=composition">
              <i class="mdi mdi-file-document-box menu-icon"></i>
              <span class="menu-title">Gestion des<br />compositions</span>
            </a>
          </li>





          <li class="nav-item">
            <a class="nav-link" href="app.php?view=question">
              <i class="mdi mdi-lightbulb-outline menu-icon"></i>
              <span class="menu-title">Gestion des questions</span>
            </a>
          </li>





          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-parametre" aria-expanded="false" aria-controls="ui-parametre">
              <i class=" mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Paramètres</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-parametre">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item">
                  <a class="nav-link" href="app.php?view=domaine">
                    Gestion des domaines
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="app.php?view=categorie">
                    Gestion des catégories
                  </a>
                </li>
              </ul>
            </div>
          </li>

          <?php } ?>





          <?php if($_SESSION['profil']->role == "Super-Administrateur") { ?>
          <li class="nav-item">
            <a class="nav-link" href="app.php?view=user">
              <i class="fa fa-users menu-icon"></i>
              <span class="menu-title">Gestion des <br /> utilisateurs</span>
            </a>
          </li>








          <li class="nav-item">
            <a class="nav-link" href="app.php?view=school">
              <i class="fa fa-home menu-icon"></i>
              <span class="menu-title">Gestion des<br />évaluations scolaires</span>
            </a>
          </li>
          <?php } ?>







          <li class="nav-item">
            <hr style="border: 1px solid black;" />

            <span class="nav-link" href="#">
              <span class="menu-title">Documentation</span>
            </span>
          </li>


          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="mdi mdi-file-document-box menu-icon"></i>
              <span class="menu-title">Tutoriel</span>
            </a>
          </li>


        </ul>
      </nav>






























      <div class="container-fluid page-body-wrapper">
        <div id="theme-settings" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <p class="settings-heading">SIDEBAR SKINS</p>
          <div class="sidebar-bg-options selected" id="sidebar-default-theme">
            <div class="img-ss rounded-circle bg-light border mr-3"></div> Default
          </div>
          <div class="sidebar-bg-options" id="sidebar-dark-theme">
            <div class="img-ss rounded-circle bg-dark border mr-3"></div> Dark
          </div>
          <p class="settings-heading mt-2">HEADER SKINS</p>
          <div class="color-tiles mx-0 px-4">
            <div class="tiles light"></div>
            <div class="tiles dark"></div>
          </div>
        </div>

































        <!-- ********* Gestion de la navigation ************ -->

        <nav class="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
          <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
            <a class="navbar-brand brand-logo-mini align-self-center d-lg-none logo_min_app" href="">
              VT
              <!-- <img src="assets/images/logo-mini.svg" alt="logo" /> -->
            </a>
            <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize">
              <i class="mdi mdi-menu"></i>
            </button>
            <ul class="navbar-nav none">
              <li class="nav-item dropdown">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-bell-outline"></i>
                  <span class="count count-varient1">7</span>
                </a>

                <div class="dropdown-menu navbar-dropdown navbar-dropdown-large preview-list" aria-labelledby="notificationDropdown">
                  <h6 class="p-3 mb-0">Notifications</h6>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face4.jpg" alt="" class="profile-pic" />
                    </div>
                    <div class="preview-item-content">
                      <p class="mb-0"> Dany Miles <span class="text-small text-muted">commented on your photo</span>
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face3.jpg" alt="" class="profile-pic" />
                    </div>
                    <div class="preview-item-content">
                      <p class="mb-0"> James <span class="text-small text-muted">posted a photo on your wall</span>
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <img src="assets/images/faces/face2.jpg" alt="" class="profile-pic" />
                    </div>
                    <div class="preview-item-content">
                      <p class="mb-0"> Alex <span class="text-small text-muted">just mentioned you in his post</span>
                      </p>
                    </div>
                  </a>
                  <div class="dropdown-divider"></div>
                  <p class="p-3 mb-0">View all activities</p>
                </div>
              </li>


              <li class="nav-item dropdown d-none d-sm-flex">
                <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-email-outline"></i>
                  <span class="count count-varient2">5</span>
                </a>

                <div class="dropdown-menu navbar-dropdown navbar-dropdown-large preview-list" aria-labelledby="messageDropdown">
                  <h6 class="p-3 mb-0">Messages</h6>
                  <a class="dropdown-item preview-item">
                    <div class="preview-item-content flex-grow">
                      <span class="badge badge-pill badge-success">Request</span>
                      <p class="text-small text-muted ellipsis mb-0"> Suport needed for user123 </p>
                    </div>
                    <p class="text-small text-muted align-self-start"> 4:10 PM </p>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-item-content flex-grow">
                      <span class="badge badge-pill badge-warning">Invoices</span>
                      <p class="text-small text-muted ellipsis mb-0"> Invoice for order is mailed </p>
                    </div>
                    <p class="text-small text-muted align-self-start"> 4:10 PM </p>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-item-content flex-grow">
                      <span class="badge badge-pill badge-danger">Projects</span>
                      <p class="text-small text-muted ellipsis mb-0"> New project will start tomorrow </p>
                    </div>
                    <p class="text-small text-muted align-self-start"> 4:10 PM </p>
                  </a>
                  <h6 class="p-3 mb-0">See all activity</h6>
                </div>
              </li>
            </ul>


            <ul class="navbar-nav navbar-nav-right ml-lg-auto">
              <!-- <li class="nav-item dropdown d-none d-xl-flex border-0">
                <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-earth"></i> French </a>
                <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
                  <a class="dropdown-item" href="#"> English </a>
                </div>
              </li> -->
              <li class="nav-item nav-profile dropdown border-0">
                <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown">

                  <!-- Chargement image de profil -->

                  <?php if($_SESSION['profil']->image == '' ||
                      $_SESSION['profil']->image == null) { ?>

                  <img class="nav-profile-img mr-2 img-responsive" alt="" src="img/avatar.png" />

                  <?php } else { ?>

                  <img class="nav-profile-img mr-2 img-responsive" alt="" src="<?php echo $res_user.$_SESSION['profil']->image ?>" />

                  <?php } ?>

                  <!-- Fin Chargement image de profil -->

                  <span class="profile-name">
                    <?php echo explode(' ', $_SESSION['profil']->nom)[0]; ?>
                  </span>
                </a>

                <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
                  <span class="dropdown-item" 
                  onclick="document.location.href='<?php echo 'app.php?view=profil&p='. $_SESSION['profil']->iduser; ?>'">
                    <i class="mdi mdi-account mr-2 text-info"></i>
                    Mon Compte
                  </span>

                  <span class="dropdown-item" onclick="document.location.href='app.php?view=mescompositions'">
                    <i class="mdi mdi-launch mr-2 text-info"></i>
                    Mes Compos
                  </span>

                  <span class="dropdown-item" onclick="document.location.href='app.php?view=mesevaluations'">
                    <i class="mdi mdi-school mr-2 text-info"></i>
                    Mes Notes
                  </span>

                  <span class="dropdown-item" onclick="document.location.href='controller/deconnexionController.php'">
                    <i class="mdi mdi-logout mr-2 text-danger"></i>
                    Déconnexion
                  </span>

                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </nav>

        <!-- ********* Fin Gestion de la navigation ************ -->






































        <!-- ********* Gestion des modules ************ -->

        <div class="main-panel">
          <div class="content-wrapper">
            <?php 

            if($view == null) {
              include('views/connexion.php');
            }
            else if($view == 'start') {
              include('views/dashboard.php');
            }


            else if($view == 'user') {
              include('views/user/index.php');
            }
            else if($view == 'user_create') {
              include('views/user/create.php');
            }
            else if($view == 'user_update') {
              include('views/user/update.php');
            }


            else if($view == 'domaine') {
              include('views/domaine/index.php');
            }
            else if($view == 'domaine_create') {
              include('views/domaine/create.php');
            }
            else if($view == 'domaine_update') {
              include('views/domaine/update.php');
            }


            else if($view == 'categorie') {
              include('views/categorie/index.php');
            }
            else if($view == 'categorie_create') {
              include('views/categorie/create.php');
            }
            else if($view == 'categorie_update') {
              include('views/categorie/update.php');
            }


            else if($view == 'question') {
              include('views/question/index.php');
            }
            else if($view == 'question_create') {
              include('views/question/create.php');
            }
            else if($view == 'question_update') {
              include('views/question/update.php');
            }



            else if($view == 'proposition') {
              include('views/proposition/index.php');
            }
            else if($view == 'proposition_create') {
              include('views/proposition/create.php');
            }
            else if($view == 'proposition_update') {
              include('views/proposition/update.php');
            }


            else if($view == 'composition') {
              include('views/composition/index.php');
            }
            else if($view == 'composition_create') {
              include('views/composition/create.php');
            }
            else if($view == 'composition_update') {
              include('views/composition/update.php');
            }
            else if($view == 'mescompositions') {
              include('views/mescompositions/index.php');
            }



            else if($view == 'evaluation') {
              include('views/evaluation/index.php');
            }
            else if($view == 'evaluation_create') {
              include('views/evaluation/create.php');
            }
            else if($view == 'evaluation_update') {
              include('views/evaluation/update.php');
            }
            else if($view == 'mesevaluations') {
              include('views/mesevaluations/index.php');
            }



            else if($view == 'profil') {
              include('views/profil.php');
            }



            else if($view == 'school') {
              include('views/school/index.php');
            }
            else if($view == 'school_create') {
              include('views/school/create.php');
            }
            else if($view == 'school_update') {
              include('views/school/update.php');
            }
            else if($view == 'school_mescompositions') {
              include('views/school/mescompositions.php');
            }


            ?>

          </div>

          <!-- ********* Fin Gestion des modules ************ -->





















          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">
                Copyright © GeScope 2021
              </span>

              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> 
                Designed by
                <a href="mailto:mtejiogni@gescope.com">
                  TEJIOGNI Marc
                </a> 
              </span>
            </div>
          </footer>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    











    <!--***************** Gestion du JavaScript ************-->

    <script src="assets/js/template.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <!-- <script type="text/javascript" src="js/app.js"></script> -->

    <!-- AJAX -->
    <script type="text/javascript" src="js/configAjax.js"></script>

  </body>
</html>