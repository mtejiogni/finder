<?php

require_once 'Database.php';

class TypedocumentDB {

    private $db;
    private $nametable;
    private $idtable;

    function __construct() {
        $this->db = new Database();
        $this->nametable= $this->db->getDbPrefix() . "typedocument";
        $this->idtable= "idtypedocument";
    }



    public function readAll() {
        $statement= 'select * from '.$this->nametable.' order by '.$this->idtable.' desc';

        $req= $this->db->prepare($statement, null);
        return $this->db->datas($req, false);
    }


    public function read($id) {
        $statement= 'select * from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }



    public function readNom($nom) {
        $statement= 'select * from '.$this->nametable.' where nom= ? order by '.$this->idtable.' desc';
        $attributes= array($nom);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }












    public function create($nom) {
        $statement= 'insert into '.$this->nametable.' set nom= ?';

        $attributes= array($nom);
        $this->db->prepare($statement, $attributes);
    }











    public function update($id, $nom) {
        $statement= 'update '.$this->nametable.' set nom= ? where '.$this->idtable.'= ?';

        $attributes= array($nom, $id);
        $req= $this->db->prepare($statement, $attributes);
    }























    public function delete($id) {
        $statement= 'delete from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
    }


} 