<?php

// Définition du fuseau horaire
date_default_timezone_set('Africa/Douala');


$protocole = dirname($_SERVER['SERVER_PROTOCOL']);
$host = $_SERVER['HTTP_HOST'];
$uri = $_SERVER['REQUEST_URI'];

if($host == 'gescope.com' || $host == 'demos.gescope.com') {
	define('HOST', 'localhost');
	define('USER', 'gescopec_username');
	define('PASSWORD', 'gescopec_username');
	define('DATABASE', 'gescopec_teachdemo');
	define('PREFIX', '');
}
else {
	define('HOST', 'localhost');
	define('USER', 'root');
	define('PASSWORD', '');
	define('DATABASE', 'finder');
	define('PREFIX', '');
}

?>
