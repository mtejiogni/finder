<?php 

require_once 'config.php';



class Database {
	private $db_host;
	private $db_user;
	private $db_pass;
    private $db_name;
	private $db_prefix;


	private $pdo;


	public function __construct() {
		$this->db_host= HOST;
		$this->db_user= USER;
		$this->db_pass= PASSWORD;
        $this->db_name= DATABASE;
		$this->db_prefix= PREFIX;
	}








	public function getPDO() {
		if ($this->pdo === null) {
			$dsn = 'mysql:dbname='.$this->db_name.';host='.$this->db_host.';charset=utf8';
			$user= $this->db_user;
			$pass= $this->db_pass;

			$pdo= new PDO($dsn, $user, $pass);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

			$this->pdo= $pdo;
		}
		return $this->pdo;
	}


	public function prepare($statement, $attributes= null) {
		$req= $this->getPDO()->prepare($statement);

        if(is_null($attributes)) {
            $req->execute();
        }
        else {
            $req->execute($attributes);
        }

		return $req;
	}


    public function datas($req, $one= false) {
        $req->setFetchMode(PDO::FETCH_OBJ);

        if($one) {
            $datas= $req->fetch();
        } else {
            $datas= $req->fetchAll();
        }

        return $datas;
    }















    /**
     * @return string
     */
    public function getDbHost()
    {
        return $this->db_host;
    }

    /**
     * @param string $db_host
     */
    public function setDbHost($db_host)
    {
        $this->db_host = $db_host;
    }

    /**
     * @return string
     */
    public function getDbName()
    {
        return $this->db_name;
    }

    /**
     * @param string $db_name
     */
    public function setDbName($db_name)
    {
        $this->db_name = $db_name;
    }

    /**
     * @return string
     */
    public function getDbPass()
    {
        return $this->db_pass;
    }

    /**
     * @param string $db_pass
     */
    public function setDbPass($db_pass)
    {
        $this->db_pass = $db_pass;
    }

    /**
     * @return string
     */
    public function getDbPrefix()
    {
        return $this->db_prefix;
    }

    /**
     * @param string $db_prefix
     */
    public function setDbPrefix($db_prefix)
    {
        $this->db_prefix = $db_prefix;
    }

    /**
     * @return string
     */
    public function getDbUser()
    {
        return $this->db_user;
    }

    /**
     * @param string $db_user
     */
    public function setDbUser($db_user)
    {
        $this->db_user = $db_user;
    }






    public function toString() {
        $str = 'Nom de la base de donnees : ' . $this->db_name . ' <br />' .
        	   'Serveur Hote : ' . $this->db_host . ' <br />' .
        	   'Nom utilisateur : ' . $this->db_user . ' <br />' .
        	   'Mot de passe : ' . $this->db_pass . ' <br />' .
        	   'Prefixe de la base de donnees : ' . $this->db_prefix;

       	return $str;
    }

}

?>