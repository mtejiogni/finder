<?php

require_once 'Database.php';

class DocumentDB {

    private $db;
    private $nametable;
    private $idtable;

    function __construct() {
        $this->db = new Database();
        $this->nametable= $this->db->getDbPrefix() . "document";
        $this->idtable= "iddocument";
    }



    public function readAll() {
        $statement= 'select * from '.$this->nametable.' order by '.$this->idtable.' desc';

        $req= $this->db->prepare($statement, null);
        return $this->db->datas($req, false);
    }


    public function read($id) {
        $statement= 'select * from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }


    public function readIdtypedocument($idtypedocument) {
        $statement= 'select * from '.$this->nametable.' where idtypedocument= ?';
        $attributes= array($idtypedocument);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readIduser($iduser) {
        $statement= 'select * from '.$this->nametable.' where iduser= ?';
        $attributes= array($iduser);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readProprietaire($proprietaire) {
        $statement= 'select * from '.$this->nametable.' where proprietaire= ?';
        $attributes= array($proprietaire);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readNom($nom) {
        $statement= 'select * from '.$this->nametable.' where nom= ?';
        $attributes= array($nom);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readNumero($numero) {
        $statement= 'select * from '.$this->nametable.' where numero= ?';
        $attributes= array($numero);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readEtat($etat) {
        $statement= 'select * from '.$this->nametable.' where etat= ?';
        $attributes= array($etat);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readDatecreation($datecreation) {
        $statement= 'select * from '.$this->nametable.' where datecreation= ?';
        $attributes= array($datecreation);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


















    public function create($idtypedocument, $iduser, $proprietaire, $nom, $numero, $etat, $datecreation, $photo) {
        $statement= 'insert into '.$this->nametable.' set idtypedocument= ?, iduser= ?, proprietaire=?, nom= ?, numero= ?, etat= ?, datecreation= ?, photo= ?';

        $attributes= array($idtypedocument, $iduser, $proprietaire, $nom, $numero, $etat, $datecreation, $photo);
        $this->db->prepare($statement, $attributes);
    }











    public function update($id, $idtypedocument, $iduser, $proprietaire, $nom, $numero, $etat, $datecreation, $photo) {
        $statement= 'update '.$this->nametable.' set idtypedocument= ?, iduser= ?, proprietaire=?, nom= ?, numero= ?, etat= ?, datecreation= ?, photo= ? where '.$this->idtable.'= ?';

        $attributes= array($idtypedocument, $iduser, $proprietaire, $nom, $numero, $etat, $datecreation, $photo, $id);
        $req= $this->db->prepare($statement, $attributes);
    }

    public function updateEtat($id, $etat) {
        $statement= 'update '.$this->nametable.' set etat= ? where '.$this->idtable.'= ?';
        $attributes= array($etat, $id);
        $req= $this->db->prepare($statement, $attributes);
    }

    public function updatePhoto($id, $photo) {
        $statement= 'update '.$this->nametable.' set photo= ? where '.$this->idtable.'= ?';
        $attributes= array($photo, $id);
        $req= $this->db->prepare($statement, $attributes);
    }























    public function delete($id) {
        $statement= 'delete from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
    }


} 