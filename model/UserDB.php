<?php

require_once 'Database.php';

class UserDB {

    private $db;
    private $nametable;
    private $idtable;

    function __construct() {
        $this->db = new Database();
        $this->nametable= $this->db->getDbPrefix() . "user";
        $this->idtable= "iduser";
    }



    public function readAll() {
        $statement= 'select * from '.$this->nametable.' order by '.$this->idtable.' desc';

        $req= $this->db->prepare($statement, null);
        return $this->db->datas($req, false);
    }


    public function read($id) {
        $statement= 'select * from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }


    public function readNom($nom) {
        $statement= 'select * from '.$this->nametable.' where nom= ?';
        $attributes= array($nom);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readPrenom($prenom) {
        $statement= 'select * from '.$this->nametable.' where prenom= ?';
        $attributes= array($prenom);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readSexe($sexe) {
        $statement= 'select * from '.$this->nametable.' where sexe= ?';
        $attributes= array($sexe);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readTelephone($telephone) {
        $statement= 'select * from '.$this->nametable.' where telephone= ?';
        $attributes= array($telephone);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }


    public function readEmail($email) {
        $statement= 'select * from '.$this->nametable.' where email= ?';
        $attributes= array($email);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }


    public function readVille($ville) {
        $statement= 'select * from '.$this->nametable.' where ville= ?';
        $attributes= array($ville);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }


    public function readPassword($password) {
        $statement= 'select * from '.$this->nametable.' where password= ?';
        $attributes= array($password);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }


    public function readCompte($email, $password) {
        $statement= 'select * from '.$this->nametable.' where email= ? or password= ?';
        $attributes= array($email, $password);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, true);
    }



    public function readRole($role) {
        $statement= 'select * from '.$this->nametable.' where role= ? order by '.$this->idtable.' desc';
        $attributes= array($role);

        $req= $this->db->prepare($statement, $attributes);
        return $this->db->datas($req, false);
    }

















    public function create($nom, $prenom, $sexe, $telephone, $email, $ville, $password, $role) {
        $statement= 'insert into '.$this->nametable.' set nom= ?, prenom= ?, sexe=?, telephone= ?, email= ?, ville= ?, password= ?, role= ?';

        $attributes= array($nom, $prenom, $sexe, $telephone, $email, $ville, $password, $role);
        $this->db->prepare($statement, $attributes);
    }











    public function update($id, $nom, $prenom, $sexe, $telephone, $email, $ville, $password, $role) {
        $statement= 'update '.$this->nametable.' set nom= ?, prenom= ?, sexe=?, telephone= ?, email= ?, ville= ?, password= ?, role= ? where '.$this->idtable.'= ?';

        $attributes= array($nom, $prenom, $sexe, $telephone, $email, $ville, $password, $role, $id);
        $req= $this->db->prepare($statement, $attributes);
    }

    public function updateRole($id, $role) {
        $statement= 'update '.$this->nametable.' set role= ? where '.$this->idtable.'= ?';
        $attributes= array($role, $id);
        $req= $this->db->prepare($statement, $attributes);
    }


    public function updateCompte($id, $email, $password) {
        $statement= 'update '.$this->nametable.' set email=?, password=? where '.$this->idtable.'= ?';
        $attributes= array($email, $password, $id);
        $req= $this->db->prepare($statement, $attributes);
    }























    public function delete($id) {
        $statement= 'delete from '.$this->nametable.' where '.$this->idtable.'= ?';
        $attributes= array($id);

        $req= $this->db->prepare($statement, $attributes);
    }


} 