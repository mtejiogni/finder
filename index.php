<?php
session_start();



/****************** Gestion des Erreurs *******************/

if(isset($_SESSION['error']) == true) {
  echo '<script type="text/javascript">';
  echo 'alert("'. $_SESSION['error'] .'");';
  echo '</script>';
  unset($_SESSION['error']);
}

/**************** Fin Gestion des Erreurs *****************/

?>



<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Finder - Connexion</title>


    <!-- ************** Gestion du CSS ************** -->
    <link rel="icon" type="image/x-icon" href="img/favicon.ico" />

    <link rel="stylesheet" type="text/css" href="css/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="css/fontawesome.css">

    <!-- ++++ Generer des alerts personnalisees -->
    <link rel="stylesheet" type="text/css" href="css/sweetalert2.min.css" />

    <!-- ++++ Select personnalise -->
    <link rel="stylesheet" type="text/css" href="css/chosen/prism.css" />
    <link rel="stylesheet" type="text/css" href="css/chosen/chosen.css" />

    <link rel="stylesheet" href="css/connexion.css">







    <!--***************** Gestion du JavaScript ***************-->

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    
    <!-- ++++ Generer des alerts personnalisees -->
    <script type="text/javascript" src="js/sweetalert2.all.min.js"></script>

    <!-- ++++ Select personnalise -->
    <script type="text/javascript" src="js/chosen/chosen.jquery.js"></script>
    <script type="text/javascript" src="js/chosen/prism.js"></script>
    <script type="text/javascript" src="js/chosen/init.js"></script>
</head>
<body>



    <!-- Gestion du formulaire de connexion -->
    <div class="container ">
        <div class="forms-container">
            <div class="signin-signup">

                <form name="form_connexion" action="controller/connexionController.php" method="POST" enctype="multipart/form-data" class="sign-in-form" id="form_connexion">

                    <h2 class="">Connectez-Vous</h2>

                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" name="login" id="login" placeholder="Nom d'utilisateur" required>
                    </div>

                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input type="password" name="password" id="password" placeholder="Mot de passe" required>
                    </div>

                    <input type="submit" value="Connexion" class="btn solid">







                    <p class="social-text">Suivez-Nous Sur</p>

                    <div class="social-media">

                        <a href="#" class="social-icon">
                            <i class="fab fa-facebook"></i>
                        </a>

                        <a href="#" class="social-icon">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="#" class="social-icon">
                            <i class="fab fa-google"></i>
                        </a>

                        <a href="#" class="social-icon">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </div>
                </form>


















                <form name="form_create" action="controller/userController.php?action=createProfil" method="POST" enctype="multipart/form-data" class="sign-up-form" id="form_inscription">
                    <h2 class="">Entrez vos informations personnelles</h2>

                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input type="text" name="nom" id="nom" placeholder="Nom & Prénom" required>
                    </div>


                    <div class="input-field">
                        <i class="fas fa-phone"></i>
                        <input type="tel" name="telephone" id="telephone" placeholder="Téléphone" required>
                    </div>


                    <div class="input-field">
                        <i class="fas fa-envelope"></i>
                        <input type="email" name="email" id="email" placeholder="Email" required>
                    </div>


                    <div class="input-field">
                        <i class="fas fa-map-marker-alt"></i>
                        <input type="text" name="adresse" id="adresse" placeholder="Adresse">
                    </div>



                    <input type="submit" value="Envoyez" class="btn solid">







                    <p class="social-text">Suivez-Nous Sur</p>

                    <div class="social-media">
                        <a href="#" class="social-icon">
                            <i class="fab fa-facebook"></i>
                        </a>
                        <a href="#" class="social-icon">
                            <i class="fab fa-twitter"></i>
                        </a>

                        <a href="#" class="social-icon">
                            <i class="fab fa-google"></i>
                        </a>

                        <a href="#" class="social-icon">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </div>
                </form>
            </div>
        </div>
























        <div class="panels-container">
            <div class="panel left-panel">
                <div class="content">
                    <h3>Finder</h3>
                    <p>
                        La Foie est une ferme assurance des choses que l'on espere et une démonstration de celle que l'on ne voit pas
                    </p>
                    <button class="btn transparent" id="sign-up-btn">
                        Je n'ai pas de compte
                    </button>
                </div>
                <img src="img/svg_connexion/undraw_flutter_dev_wvqj.svg" class="image" alt="">
            </div>

            <div class="panel right-panel">
                <div class="content">
                    <h3>QUI SOMMES NOUS ?</h3>
                    <p>
                        La Foie est une ferme assurance des choses que l'on espere et une démonstration de celle que l'on ne voit pas
                    </p>
                    <button class="btn transparent" id="sign-in-btn">
                        J'ai déjà un compte
                    </button>
                </div>
                <img src="img/svg_connexion/undraw_On_the_way_re_swjt.svg" class="image" alt="">
            </div>
        </div>
    </div>
    <!-- Fin de gestion du formualaire de connexion -->







    <script src="js/connexion.js"></script>
</body>

</html>