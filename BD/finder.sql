
# -----------------------------------------------
#             creation de la base de donnees
# -----------------------------------------------
drop database if exists finder;
create database if not exists finder character set utf8;
use finder;













# -----------------------------------
#       Creation des tables
# -----------------------------------

drop table if exists typedocument;
create table if not exists typedocument (
  idtypedocument integer not null auto_increment,
  nom varchar(128),
  primary key (idtypedocument)
) 
character set utf8;






drop table if exists user;
create table if not exists user (
  iduser integer not null auto_increment,
  nom varchar(128),
  prenom varchar(128),
  sexe varchar(128),
  telephone varchar(128),
  email varchar(128),
  ville varchar(128),
  password varchar(128),
  role varchar(128), # Client, Admin
  primary key (iduser)
) 
character set utf8;





drop table if exists document;
create table if not exists document (
  iddocument integer not null auto_increment,
  idtypedocument integer not null,
  iduser integer not null,
  proprietaire integer null,
  nom text,
  numero integer,
  etat varchar(128), # Actif, Inactif
  datecreation varchar(128),
  photo text,
  primary key (iddocument),
  foreign key (idtypedocument) references typedocument(idtypedocument),
  foreign key (iduser) references user(iduser)
) 
character set utf8;



















# -----------------------------------
#       Requetes Insertion
# -----------------------------------


# -----------> typedocument

insert into typedocument (nom) values
  ("CNI"),
  ("Passeport"),
  ("Diplôme"),
  ("Acte de naissance");




# -----------> user

insert into user set
  nom= "TEJIOGNI",
  prenom= "Marc",
  sexe= "M",
  telephone= "+237693909121",
  email= "mtejiogni@gmail.com",
  ville= "Douala",
  password= "root",
  role= "Admin";


insert into user set
  nom= "DEFFO",
  prenom= "Steve",
  sexe= "M",
  telephone= "+237656451946",
  email= "deffongankamsteve@yahoo.com",
  ville= "Douala",
  password= "1234",
  role= "Admin";


