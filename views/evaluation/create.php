<?php

$users= $userdb->readAll();
$compositions= $compositiondb->readAll();

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Ajouter une évaluation
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=evaluation'">
			Liste des évaluations
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur l'évaluation
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/evaluationController.php?action=create">

					<div class="form-group">
					    <label for="iduser">
					    	Sélectionnez un utilisateur
					    </label>

					    <select name="iduser" id="iduser" class="form-control">
					    	<?php 
					    	foreach ($users as $user) {
					    	?>
					    	
					    	<option value="<?php echo $user->iduser ?>">
					    		<?php echo $user->nom ?> (<?php echo $user->telephone ?>)
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="idcomposition">
					    	Sélectionnez une composition
					    </label>

					    <select name="idcomposition" id="idcomposition" class="form-control">
					    	<?php 
					    	foreach ($compositions as $composition) {
					    	?>
					    	
					    	<option value="<?php echo $composition->idcomposition ?>">
					    		<?php echo $composition->intitule ?> (<?php echo $composition->code ?>)
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="note">Score</label>
					    <input type="number" name="note" class="form-control" id="note" required />
					</div>


					<div class="form-group">
					    <label for="nbpoints">Nombre de points</label>
					    <input type="number" name="nbpoints" class="form-control" id="nbpoints" required />
					</div>


					<button type="submit" class="btn btn-primary mr-2 float-right">
						Enregistrer
					</button>
				</form>
			</div>
		</div>
	</div>
</div>