<?php

$users= $userdb->readAll();
$compositions= $compositiondb->readAll();

$idevaluation= $_GET['p'];
$evaluation= $evaluationdb->read($idevaluation);
$user= $userdb->read($evaluation->iduser);
$composition= $compositiondb->read($evaluation->idcomposition);

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier une évalution
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=evaluation'">
			Liste des évaluations
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur l'évaluation
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/evaluationController.php?action=update">
					<div class="form-group none">
					    <label for="idevaluation">ID</label>
					    <input type="text" name="idevaluation" class="form-control" id="idevaluation" placeholder="ID" required value="<?php echo $evaluation->idevaluation; ?>" />
					</div>



					<div class="form-group">
					    <label for="iduser">
					    	Sélectionnez un utilisateur
					    </label>

					    <select name="iduser" id="iduser" class="form-control">
					    	<option value="<?php echo $user->iduser ?>" class="none" selected>
					    		<?php echo $user->nom ?> (<?php echo $user->telephone ?>)
					    	</option>

					    	<?php 
					    	foreach ($users as $user) {
					    	?>
					    	
					    	<option value="<?php echo $user->iduser ?>">
					    		<?php echo $user->nom ?> (<?php echo $user->telephone ?>)
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>



					<div class="form-group">
					    <label for="idcomposition">
					    	Sélectionnez une composition
					    </label>

					    <select name="idcomposition" id="idcomposition" class="form-control">
					    	<option value="<?php echo $composition->idcomposition ?>" class="none" selected>
					    		<?php echo $composition->intitule ?> (<?php echo $composition->code ?>)
					    	</option>

					    	<?php 
					    	foreach ($compositions as $composition) {
					    	?>
					    	
					    	<option value="<?php echo $composition->idcomposition ?>">
					    		<?php echo $composition->intitule ?> (<?php echo $composition->code ?>)
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="note">Score</label>
					    <input type="number" name="note" class="form-control" id="note" required value="<?php echo $evaluation->note; ?>" />
					</div>


					<div class="form-group">
					    <label for="nbpoints">Nombre de points</label>
					    <input type="number" name="nbpoints" class="form-control" id="nbpoints" required value="<?php echo $evaluation->nbpoints; ?>" />
					</div>

					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>