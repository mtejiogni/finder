<?php

$evaluations= $evaluationdb->readAll();
$users= $userdb->readAll();
$compositions= $compositiondb->readAll();

?>

<script type="text/javascript">
	/*Swal.fire({
	  icon: 'error',
	  title: 'Oops...',
	  html: 'Something went <br /> wrong!',
	  footer: '<a href="">Why do I have this issue?</a>',
	  showConfirmButton: false
	}); */
</script>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des évaluations
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=evaluation_create'">
			Ajouter une évaluation
		</button>
	</div>
</div>



<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des évaluations
	    			(<?php echo sizeof($evaluations); ?>)
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-bordered">
			            <thead>
			            	<tr>
				                <th>#</th>
				                <th>Utilisateur</th>
				                <th>Composition</th>
				                <th>Score</th>
				                <th>Nb pts</th>
				                <th>Heure</th>
				                <th>Date</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($evaluations != null && sizeof($evaluations) != 0) {
								$i= 0;

								foreach($evaluations as $evaluation) {
									$user= $userdb->read($evaluation->iduser);
									$composition= $compositiondb->read($evaluation->idcomposition);
									$categorie= $categoriedb->read($composition->idcategorie);
									$domaine= $domainedb->read($categorie->iddomaine);

									$update= 'app.php?view=evaluation_update&p='. $evaluation->idevaluation;
									$delete= 'controller/evaluationController.php?action=delete&p='. $evaluation->idevaluation;
									$i= $i+1;
							?>

			            	<tr class="element">
				                <td class="data"><?php echo $i; ?></td>






				                <td class="">
				                	<span class="btn btn-info" id="<?php echo 'info_user'. $i ?>">
				                		<?php echo $user->nom; ?>
				                	</span>
				                </td>

				                <script type="text/javascript">
				                	$('body').on('click', '<?php echo '#info_user'. $i ?>', function() {
				                		$user= <?php echo json_encode($user); ?>;
				                	
					                	$html= 	'<div style="text-align:left; line-height:30px;">' +
			                						'<b>Nom :</b> <i>' + $user.nom + '</i><br />' +
			                						'<b>Numéro de téléphone :</b> <i>' + $user.telephone + '</i><br />' +
			                						'<b>Email :</b> <i>' + $user.email + '</i><br />' +
			                						'<b>Adresse :</b> <i>' + $user.adresse + '</i><br />' +
			                					'</div>';

				                		Swal.fire({
										  title: 'Utilisateur',
										  html: $html,
										  showConfirmButton: false
										});
				                	});
								</script>

				                <td class="data none"><?php echo $user->nom; ?></td>








				                <td class="">
				                	<span class="btn btn-info" id="<?php echo 'info_composition'. $i ?>">
				                		<?php echo $composition->intitule; ?>
				                	</span>
				                </td>

				                <script type="text/javascript">
				                	$('body').on('click', '<?php echo '#info_composition'. $i ?>', function() {
				                		$composition= <?php echo json_encode($composition); ?>;
					                	$categorie= <?php echo json_encode($categorie); ?>;
					                	$domaine= <?php echo json_encode($domaine); ?>;

					                	$html= 	'<div style="text-align:left; line-height:30px;">' +
		                							'<b>Domaine :</b> <i>' + $domaine.intitule + '</i><br />' +
	                								'<b>Catégorie :</b> <i>' + $categorie.intitule + '</i><br />' +
	                								'<b>Code :</b> <i>' + $composition.code + '</i><br />' +
	                								'<b>Intitulé :</b> <i>' + $composition.intitule + '</i><br />' +
	                								'<b>Durée :</b> <i>' + $composition.duree + 'H</i><br />' +
	                								'<b>Nb questions :</b> <i>' + $composition.nbquestions + '</i><br />' +
	                							'</div>';

				                		Swal.fire({
										  title: 'Composition',
										  html: $html,
										  showConfirmButton: false
										});
				                	});
								</script>

				                <td class="data none"><?php echo $composition->intitule; ?></td>



				                <td class="data"><?php echo $evaluation->note; ?></td>
				                <td class="data"><?php echo $evaluation->nbpoints; ?></td>
				                <td class="data"><?php echo $evaluation->heure; ?></td>
				                <td class="data"><?php echo $evaluation->jour; ?></td>

				                <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
			            	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>