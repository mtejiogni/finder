<?php

$iduser= $_GET['p'];
$user= $userdb->read($iduser);

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Mon Compte
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur mon compte
				</h4>



				<form class="forms-sample" name="form" method="POST" action="controller/userController.php?action=updateProfil" enctype="multipart/form-data">

					
					<div class="form-group">
						<label for="image">
							Changer l'image de profil
						</label>

						<p class="text-center">
							<?php if($user->image == '' || $user->image == null) { ?>

			                <img src="img/avatar.png" alt="Image" class="img-responsive" />

			                <?php } else { ?>

			                <img src="<?php echo $res_user.$user->image ?>" alt="Image" class="img-responsive" />

			                <?php } ?>
						</p>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>






					<div class="form-group none">
					    <label for="iduser">ID UTILISATEUR</label>
					    <input type="text" name="iduser" class="form-control" id="iduser" placeholder="ID" required value="<?php echo $user->iduser; ?>" />
					</div>


					<div class="form-group">
					    <label for="nom">Nom</label>
					    <input type="text" name="nom" class="form-control" id="nom" placeholder="Entrez le nom" required value="<?php echo $user->nom; ?>" />
					</div>


					<div class="form-group">
					    <label for="telephone">Téléphone</label>
					    <input type="tel" name="telephone" class="form-control" id="telephone" placeholder="Numéro de téléphone" required value="<?php echo $user->telephone; ?>" />
					</div>


					<div class="form-group">
					    <label for="email">Email</label>
					    <input type="email" name="email" class="form-control" id="email" placeholder="Adresse Email" required value="<?php echo $user->email; ?>" />
					</div>


					<div class="form-group">
					    <label for="adresse">Adresse</label>
					    <textarea name="adresse" class="form-control" id="adresse" placeholder="Entrez votre localisation"><?php echo $user->adresse; ?></textarea>
					</div>


					<div class="form-group">
					    <label for="login">Nom d'utilisateur</label>
					    <input type="text" name="login" class="form-control" id="login" placeholder="Entrez le nom d'utilisateur" required value="<?php echo $user->login; ?>" />
					</div>


					<div class="form-group">
					    <label for="password">Mot de passe</label>
					    <input type="password" name="password" class="form-control" id="password" placeholder="Entrez le mot de passe" required value="<?php echo $user->password; ?>" />
					</div>




					<button type="submit" class="btn btn-primary mr-2 float-right">
						Enregistrer
					</button>

				</form>
			</div>
		</div>
	</div>
</div>




