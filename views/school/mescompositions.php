<?php

$users= $userdb->readAll();
$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();

$compositions= $compositiondb->readEtat('Activé');

?>



<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Mes Compositions
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des Compositions
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br /><br />


		     	<div class="row elements">
                	<?php
					if($compositions != null && sizeof($compositions) != 0) {
						$i= 0;

						foreach($compositions as $composition) {
							$user= $userdb->read($composition->iduser);
							$categorie= $categoriedb->read($composition->idcategorie);
							$domaine= $domainedb->read($categorie->iddomaine);


							$evaluation= 'school.php?p='. $composition->idcomposition;

							$update= 'app.php?view=composition_update&p='. $composition->idcomposition;
							$delete= 'controller/compositionController.php?action=delete&p='. $composition->idcomposition;
							$i= $i+1;
					?>

                	<div class="col-md-3 col-sm-6 element">
                		<b class="data">#<?php echo $composition->code; ?></b>
                		<br /><br />

                		<?php if($composition->image == '' || $composition->image == null) { ?>

            			<div class="text-center img" style="background:url('img/avatar.png') no-repeat center; height:100px;">
            			</div>

                		<?php } else { ?>

                		<div class="text-center img" style="background:url('<?php echo $res_composition.$composition->image ?>') no-repeat center; height:100px;">
                		</div>
			            <?php } ?>
		                

                		<hr />

                		<h5 class="data text-center"><?php echo $composition->intitule; ?></h5>
                		<i class="data"><?php echo $composition->description; ?></i>

                		<br /><br />

                		Domaine : <span class="data"><?php echo $domaine->intitule; ?></span>
                		<br />
                		Catégorie : <span class="data"><?php echo $categorie->intitule; ?></span>
                		<br />
                		Durée : <span class="data"><?php echo $composition->duree; ?></span> H
                		<br />
                		NB Questions : <span class="data"><?php echo $composition->nbquestions; ?></span>

                		<br /><br />

                		<p class="data">Proposé par : <?php echo $user->nom; ?></p>

                		<div class="text-center">
	                		<button class="btn btn-success" onclick="window.open('<?php echo $evaluation; ?>')">
	                			Démarrer la composition
	                		</button>
                		</div>
                	</div>


                	<?php
						}
					}
					?>
                </div>








	      	</div>
	    </div>
	</div>
</div>