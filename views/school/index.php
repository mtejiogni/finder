<?php

$schools= $schooldb->readAll();
$compositions= $compositiondb->readAll();

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des évaluations
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>


		<button type="button" class="btn btn-sm ml-3 btn-info" onclick="document.location.href='app.php?view=school_mescompositions'">
			Lancer une composition
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=school_create'">
			Ajouter une évaluation
		</button>
	</div>
</div>



<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des évaluations
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-bordered">
			            <thead>
			            	<tr>
				                <th>#</th>
				                <th>Etudiant</th>
				                <th>Composition</th>
				                <th>Score</th>
				                <th>Nb pts</th>
				                <th>Heure</th>
				                <th>Date</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($schools != null && sizeof($schools) != 0) {
								$i= 0;

								foreach($schools as $school) {
									$composition= $compositiondb->read($school->idcomposition);
									$categorie= $categoriedb->read($composition->idcategorie);
									$domaine= $domainedb->read($categorie->iddomaine);

									$update= 'app.php?view=school_update&p='. $school->idschool;
									$delete= 'controller/schoolController.php?action=delete&p='. $school->idschool;
									$i= $i+1;
							?>

			            	<tr class="element">
				                <td class="data"><?php echo $i; ?></td>






				                <td class="">
				                	<span class="btn btn-info" id="<?php echo 'info_etudiant'. $i ?>">
				                		<?php echo $school->nom; ?>
				                	</span>
				                </td>

				                <script type="text/javascript">
				                	$('body').on('click', '<?php echo '#info_etudiant'. $i ?>', function() {
				                		$school= <?php echo json_encode($school); ?>;
				                	
					                	$html= 	'<div style="text-align:left; line-height:30px;">' +
			                						'<b>Nom :</b> <i>' + $school.nom + '</i><br />' +
			                						'<b>Matricule :</b> <i>' + $school.matricule + '</i><br />' +
			                						'<b>Spécialité :</b> <i>' + $school.specialite + '</i><br />' +
			                						'<b>Niveau :</b> <i>' + $school.niveau + '</i><br />' +
			                						'<b>Ecole :</b> <i>' + $school.ecole + '</i><br />' +
			                						'<b>Ville :</b> <i>' + $school.ville + '</i><br />' +
			                						'<b>Numéro de téléphone :</b> <i>' + $school.telephone + '</i><br />' +
			                					'</div>';

				                		Swal.fire({
										  title: 'Etudiant',
										  html: $html,
										  showConfirmButton: false
										});
				                	});
								</script>

				                <td class="data none"><?php echo $school->nom; ?></td>
				                <td class="data none"><?php echo $school->matricule; ?></td>
				                <td class="data none"><?php echo $school->specialite; ?></td>
				                <td class="data none"><?php echo $school->niveau; ?></td>
				                <td class="data none"><?php echo $school->ecole; ?></td>
				                <td class="data none"><?php echo $school->ville; ?></td>








				                <td class="">
				                	<span class="btn btn-info" id="<?php echo 'info_composition'. $i ?>">
				                		<?php echo $composition->intitule; ?>
				                	</span>
				                </td>

				                <script type="text/javascript">
				                	$('body').on('click', '<?php echo '#info_composition'. $i ?>', function() {
				                		$composition= <?php echo json_encode($composition); ?>;
					                	$categorie= <?php echo json_encode($categorie); ?>;
					                	$domaine= <?php echo json_encode($domaine); ?>;

					                	$html= 	'<div style="text-align:left; line-height:30px;">' +
		                							'<b>Domaine :</b> <i>' + $domaine.intitule + '</i><br />' +
	                								'<b>Catégorie :</b> <i>' + $categorie.intitule + '</i><br />' +
	                								'<b>Code :</b> <i>' + $composition.code + '</i><br />' +
	                								'<b>Intitulé :</b> <i>' + $composition.intitule + '</i><br />' +
	                								'<b>Durée :</b> <i>' + $composition.duree + 'H</i><br />' +
	                								'<b>Nb questions :</b> <i>' + $composition.nbquestions + '</i><br />' +
	                							'</div>';

				                		Swal.fire({
										  title: 'Composition',
										  html: $html,
										  showConfirmButton: false
										});
				                	});
								</script>

				                <td class="data none"><?php echo $composition->intitule; ?></td>



				                <td class="data"><?php echo $school->note; ?></td>
				                <td class="data"><?php echo $school->nbpoints; ?></td>
				                <td class="data"><?php echo $school->heure; ?></td>
				                <td class="data"><?php echo $school->jour; ?></td>

				                <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
			            	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>