<?php

$compositions= $compositiondb->readAll();

$idschool= $_GET['p'];
$school= $schooldb->read($idschool);
$composition= $compositiondb->read($school->idcomposition);

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier une évalution
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=school'">
			Liste des évaluations
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur l'évaluation
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/schoolController.php?action=update">
					<div class="form-group none">
					    <label for="idschool">ID</label>
					    <input type="text" name="idschool" class="form-control" id="idschool" placeholder="ID" required value="<?php echo $school->idschool; ?>" />
					</div>



					<div class="form-group">
					    <label for="idcomposition">
					    	Sélectionnez une composition
					    </label>

					    <select name="idcomposition" id="idcomposition" class="form-control">
					    	<option value="<?php echo $composition->idcomposition ?>" class="none" selected>
					    		<?php echo $composition->intitule ?> (<?php echo $composition->code ?>)
					    	</option>

					    	<?php 
					    	foreach ($compositions as $composition) {
					    	?>
					    	
					    	<option value="<?php echo $composition->idcomposition ?>">
					    		<?php echo $composition->intitule ?> (<?php echo $composition->code ?>)
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="nom">Nom & Prénom</label>
					    <input type="text" name="nom" class="form-control" id="nom" required value="<?php echo $school->nom; ?>" />
					</div>


					<div class="form-group">
					    <label for="nom">Matricule</label>
					    <input type="text" name="matricule" class="form-control" id="matricule" required value="<?php echo $school->matricule; ?>" />
					</div>


					<div class="form-group">
					    <label for="specialite">
					    	Sélectionnez une spécialité
					    </label>

					    <select name="specialite" id="specialite" class="form-control">
					    	<option value="<?php echo $school->specialite ?>" class="none" selected>
					    		<?php echo $school->specialite ?>
					    	</option>

					    	<optgroup label="Génie Informatique">
					    		<option value="Génie Logiciel (GL)">
					    			Génie Logiciel (GL)
					    		</option>
					    		<option value="Maintenance des Systèmes Informatique (MSI)">
					    			Maintenance des Systèmes Informatique (MSI)
					    		</option>
					    		<option value="Informatique Industrielle et Automatique (AII)">
					    			Informatique Industrielle et Automatique (AII)
					    		</option>
					    	</optgroup>

					    	<optgroup label="Réseaux et Télécommunications">
					    		<option value="Télécommunication (TEL)">
					    			Télécommunication (TEL)
					    		</option>
					    		<option value="Réseaux et Sécurité (RS)">
					    			Réseaux et Sécurité (RS)
					    		</option>
					    	</optgroup>

					    	<optgroup label="Gestion">
					    		<option value="Gestion des Systèmes d'Information (GSI)">
					    			Gestion des Systèmes d'Information (GSI)
					    		</option>
					    		<option value="Gestion Logistique et Transport (GLT)">
					    			Gestion Logistique et Transport (GLT)
					    		</option>
					    	</optgroup>

					    	<optgroup label="Art Métier de la Culture">
					    		<option value="Infographie et Web Design (IWD)">
					    			Infographie et Web Design (IWD)
					    		</option>
					    	</optgroup>
					    </select>
					</div>


					<div class="form-group">
					    <label for="niveau">
					    	Sélectionnez un niveau
					    </label>

					    <select name="niveau" id="niveau" class="form-control">
					    	<option value="<?php echo $school->niveau ?>" class="none" selected>
					    		<?php echo $school->niveau ?>
					    	</option>

					    	<option value="Niveau 1">Niveau 1</option>
					    	<option value="Niveau 2">Niveau 2</option>
					    	<option value="Niveau 3">Niveau 3</option>
					    	<option value="Niveau 4">Niveau 4</option>
					    	<option value="Niveau 5">Niveau 5</option>
					    </select>
					</div>


					<div class="form-group">
					    <label for="ecole">
					    	Sélectionnez une école
					    </label>

					    <select name="ecole" id="ecole" class="form-control">
					    	<option value="<?php echo $school->ecole ?>" class="none" selected>
					    		<?php echo $school->ecole ?>
					    	</option>

					    	<option value="IUEs/INSAM">IUEs/INSAM</option>
					    	<option value="IUC">IUC</option>
					    	<option value="IUGET">IUGET</option>
					    	<option value="ISTAMA">ISTAMA</option>
					    	<option value="ISTG ISIMA">ISTG ISIMA</option>
					    </select>
					</div>


					<div class="form-group">
					    <label for="ville">
					    	Sélectionnez une ville
					    </label>

					    <select name="ville" id="ville" class="form-control">
					    	<option value="<?php echo $school->ville ?>" class="none" selected>
					    		<?php echo $school->ville ?>
					    	</option>

					    	<option value="Bafoussam">Bafoussam</option>
					    	<option value="Douala">Douala</option>
					    	<option value="Dschang">Dschang</option>
					    	<option value="Kribi">Kribi</option>
					    	<option value="Yaoundé">Yaoundé</option>
					    </select>
					</div>


					<div class="form-group">
					    <label for="telephone">Numéro de téléphone</label>
					    <input type="tel" name="telephone" class="form-control" id="telephone" required value="<?php echo $school->telephone; ?>" />
					</div>


					<div class="form-group">
					    <label for="note">Score</label>
					    <input type="number" name="note" class="form-control" id="note" required value="<?php echo $school->note; ?>" />
					</div>


					<div class="form-group">
					    <label for="nbpoints">Nombre de points</label>
					    <input type="number" name="nbpoints" class="form-control" id="nbpoints" required value="<?php echo $school->nbpoints; ?>" />
					</div>


					<div class="form-group">
					    <label for="temps">Temps de composition</label>
					    <input type="time" name="temps" class="form-control" id="temps" min="00:00:00" required value="<?php echo $school->temps; ?>" />
					</div>


					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>