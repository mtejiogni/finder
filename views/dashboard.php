<?php

/******** Chargement des évaluations **********/
$evaluations= array();

if($_SESSION['profil']->role == 'Apprenant') {
  $evaluations= $evaluationdb->readIduser($_SESSION['profil']->iduser);
}
else {
  $evaluations= $evaluationdb->readAll();
}









/******** Chargement des compositions **********/
$compositions= array();

if($evaluations != null && sizeof($evaluations) > 0) {
  $chaine= array();

  foreach($evaluations as $evaluation) {
    $composition= $compositiondb->read($evaluation->idcomposition);

    if(in_array($composition->idcomposition, $chaine) == false) {
      array_push($chaine, $composition->idcomposition);

      array_push($compositions, $composition);
    }
  }
}






/******** Chargement du graphe (chart) **********/
$chart_compositions= array();
$chart_moyennes= array();


if($compositions != null && sizeof($compositions) > 0) {
  foreach($compositions as $composition) {
    $note= 0;
    $nbpoints= 0;
    $moyenne= 0.0;

    array_push($chart_compositions, $composition->intitule);
    $evals= $evaluationdb->readIdcomposition($composition->idcomposition); //evaluations

    foreach($evals as $eval) {
      $note= $note + floatval($eval->note);
      $nbpoints= $nbpoints + intval($eval->nbpoints);
    }

    $moyenne= round(($note/$nbpoints)*100, 2);
    array_push($chart_moyennes, $moyenne);
  }
}









/******** Chargement du Taux de réussite globale **********/
$taux= 0;

if($chart_moyennes != null && sizeof($chart_moyennes) > 0) {
  $som= 0.0;

  for($i=0; $i<sizeof($chart_moyennes); $i++) { 
    $som= $som + $chart_moyennes[$i];
  }

  $taux= round($som/sizeof($chart_moyennes), 2);
}



?>






<div class="page-header flex-wrap">
  <h3 class="mb-0"> Hi, Bienvenue <span class="pl-0 h6 pl-sm-2 text-muted d-inline-block"><?php echo $_SESSION['profil']->nom; ?></span>
  </h3>
  <div class="d-flex">
    <button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=mescompositions'">
      Démarrer une composition
    </button>
  </div>
</div>




























<div class="row">
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="card bg-warning">
      <div class="card-body px-3 py-4">
        <div class="d-flex justify-content-between align-items-start">
          <div class="color-card">
            <p class="mb-0 color-card-head">Compositions</p>
            <h2 class="text-white"><?php echo sizeof($compositions); ?></h2>
          </div>
          <i class="card-icon-indicator mdi mdi-file-document-box bg-inverse-icon-warning"></i>
        </div>
        <!-- <h6 class="text-white">18.33% Since last month</h6> -->
      </div>
    </div>
  </div>





  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="card bg-primary">
      <div class="card-body px-3 py-4">
        <div class="d-flex justify-content-between align-items-start">
          <div class="color-card">
            <p class="mb-0 color-card-head">Evaluations</p>
            <h2 class="text-white"><?php echo sizeof($evaluations); ?></h2>
          </div>
          <i class="card-icon-indicator mdi mdi-school bg-inverse-icon-primary"></i>
        </div>
        <!-- <h6 class="text-white">67.98% Since last month</h6> -->
      </div>
    </div>
  </div>





  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
    <div class="card bg-success">
      <div class="card-body px-3 py-4">
        <div class="d-flex justify-content-between align-items-start">
          <div class="color-card">
            <p class="mb-0 color-card-head">Taux de réussite</p>
            <h2 class="text-white"><?php echo $taux; ?><span class="h5">%</span></h2>
          </div>
          <i class="card-icon-indicator mdi mdi-briefcase-outline bg-inverse-icon-success"></i>
        </div>
        <!-- <h6 class="text-white">20.32% Since last month</h6> -->
      </div>
    </div>
  </div>



</div>











<br /><br />










<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="chart_dashboard"></div>
  </div>
</div>




<script type="text/javascript">
  
  Highcharts.chart('chart_dashboard', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Taux de réussite'
    },
    xAxis: {
        categories: <?php echo json_encode($chart_compositions); ?>
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Taux de réussite (%)'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: [
        {
            name: 'Taux de réussite (%)',
            data: <?php echo json_encode($chart_moyennes); ?>
        }
    ]
  });


</script>

            