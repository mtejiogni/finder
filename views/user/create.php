<?php

$domaines= $domainedb->readAll();

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Ajouter un Utilisateur
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=user'">
			Liste des Utilisateurs
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur l'Utilisateur
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/userController.php?action=create" enctype="multipart/form-data">
					<div class="form-group">
					    <label for="nom">Nom</label>
					    <input type="text" name="nom" class="form-control" id="nom" placeholder="Entrez le nom" required />
					</div>

					<div class="form-group">
					    <label for="telephone">Téléphone</label>
					    <input type="tel" name="telephone" class="form-control" id="telephone" placeholder="Numéro de téléphone" required />
					</div>

					<div class="form-group">
					    <label for="email">Email</label>
					    <input type="email" name="email" class="form-control" id="email" placeholder="Adresse Email" required />
					</div>

					<div class="form-group">
					    <label for="adresse">Adresse</label>
					    <textarea name="adresse" class="form-control" id="adresse" placeholder="Entrez votre localisation"></textarea>
					</div>

					<div class="form-group">
					    <label for="login">Nom d'utilisateur</label>
					    <input type="text" name="login" class="form-control" id="login" placeholder="Entrez le nom d'utilisateur" required />
					</div>

					<div class="form-group">
					    <label for="password">Mot de passe</label>
					    <input type="password" name="password" class="form-control" id="password" placeholder="Entrez le mot de passe" required />
					</div>


					<div class="form-group">
					    <label for="role">Rôle</label>
					    <select name="role" class="form-control" id="role">
					    	<option value="Super-Administrateur">
					    		Super-Administrateur
					    	</option>
					    	<option value="Administrateur">
					    		Administrateur
					    	</option>
					    	<option value="Apprenant">
					    		Apprenant
					    	</option>
					    </select>
					</div>



					<div class="form-group">
					    <label for="iddomaine">
					    	Sélectionnez le domaine de formation
					    </label>

					    <select name="iddomaine" id="iddomaine" class="form-control">
					    	<?php 
					    	foreach ($domaines as $domaine) {
					    	?>
					    	
					    	<option value="<?php echo $domaine->iddomaine ?>">
					    		<?php echo $domaine->intitule ?>
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>




					<div class="form-group">
						<label for="image">
							Ajoutez une image de profil
						</label>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>

					<button type="submit" class="btn btn-primary mr-2 float-right">
						Enregistrer
					</button>
				</form>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
	$('body').on('click change', '#role', function() {
	    $role= $(this).find('option:selected').val();
	    
	    if($role == 'Apprenant') {
	    	$('#iddomaine').parent().removeClass('none');
	    }
	    else {
	    	$('#iddomaine').parent().addClass('none');
	    }
	});

	$('#role').trigger('click');
</script>


