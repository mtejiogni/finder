<?php

$domaines= $domainedb->readAll();
$users= $userdb->readAll();

?>



<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des Utlisateurs
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=user_create'">
			Ajouter un Utilisateur
		</button>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des Utilisateurs
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-striped">
			            <thead>
			            	<tr>
				                <th>User</th>
				                <th>Nom</th>
				                <th>Téléphone</th>
				                <th>Email</th>
				                <th>Rôle</th>
				                <th>Domaine</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($users != null && sizeof($users) != 0) {
								$i= 0;

								foreach($users as $user) {
									$domaine= null;
									if($user->role == 'Apprenant') {
										$domaine= $domainedb->read($user->iddomaine);
									}
									
									$update= 'app.php?view=user_update&p='. $user->iduser;
									$delete= 'controller/userController.php?action=delete&p='. $user->iduser;
									$i= $i+1;
							?>

							<tr class="element">
	                            <td class="py-1">
	                            	<?php if($user->image == '' || $user->image == null) { ?>

					                <img src="img/avatar.png" alt="Image" />

					                <?php } else { ?>

					                <img src="<?php echo $res_user.$user->image ?>" alt="Image" />

					                <?php } ?>
	                            </td>

	                            <td class="data"><?php echo $user->nom; ?></td>
	                            <td class="data"><?php echo $user->telephone; ?></td>
	                            <td class="data"><?php echo $user->email; ?></td>
	                            <td class="data"><?php echo $user->role; ?></td>

	                            <?php if($domaine != null) { ?>
	                            <td class="data"><?php echo $domaine->intitule; ?></td>
	                            <?php } else { ?>
	                            	<td>All</td>
	                            <?php } ?>


	                            <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
                        	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>