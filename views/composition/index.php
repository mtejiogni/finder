<?php

$users= $userdb->readAll();
$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();
$compositions= $compositiondb->readAll();

?>



<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des Compositions
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=composition_create'">
			Ajouter une Composition
		</button>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des Compositions
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-striped">
			            <thead>
			            	<tr>
				                <th>Logo</th>
				                <th>Enregistrée par</th>
				                <th>Domaine</th>
				                <th>Catégorie</th>
				                <th>Code</th>
				                <th>Intitulé</th>
				                <th>Durée</th>
				                <th>NB de questions</th>
				                <th>Etat</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($compositions != null && sizeof($compositions) != 0) {
								$i= 0;

								foreach($compositions as $composition) {
									$user= $userdb->read($composition->iduser);
									$categorie= $categoriedb->read($composition->idcategorie);
									$domaine= $domainedb->read($categorie->iddomaine);


									$class_etat= '';
									if($composition->etat == 'Activé') {
										$class_etat= 'btn btn-success';
									}
									else if($composition->etat == 'Désactivé') {
										$class_etat= 'btn btn-danger';
									}

									$etat= 'controller/compositionController.php?action=updateEtat&p='. $composition->idcomposition .'&etat=';

									$update= 'app.php?view=composition_update&p='. $composition->idcomposition;
									$delete= 'controller/compositionController.php?action=delete&p='. $composition->idcomposition;
									$i= $i+1;
							?>

							<tr class="element">
	                            <td class="py-1">
	                            	<?php if($composition->image != '' || $composition->image != null) { ?>

					                <img src="<?php echo $res_composition.$composition->image ?>" alt="Image" />

					                <?php } ?>
	                            </td>



	                            <td class="data"><?php echo $user->nom; ?></td>
	                            <td class="data"><?php echo $domaine->intitule; ?></td>
	                            <td class="data"><?php echo $categorie->intitule; ?></td>
	                            <td class="data"><?php echo $composition->code; ?></td>
	                            <td class="data"><?php echo $composition->intitule; ?></td>
	                            <td class="data"><?php echo $composition->duree; ?></td>
	                            <td class="data"><?php echo $composition->nbquestions; ?></td>



	                            <td>
	                            	<div class="btn-group" role="group">
									    <button type="button" class="<?php echo $class_etat; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    	<?php echo $composition->etat; ?>
									    	<span class="caret"></span>
									    </button>

									    <ul class="dropdown-menu text-center">
									    	<li class="btn btn-success btn-xs" onclick="document.location.href='<?php echo $etat . 'Activé' ?>'">
									    		<span>
									    			Activé
									    		</span>
									    	</li>
									    	<li class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $etat . 'Désactivé' ?>'">
									    		<span>
									    			Désactivé
									    		</span>
									    	</li>
									    </ul>
									</div>
	                            </td>
	                            <td class="data none"><?php echo $composition->etat; ?></td>



	                            <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
                        	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>