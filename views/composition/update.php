<?php

$users= $userdb->readAll();
$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();
$compositions= $compositiondb->readAll();

$idcomposition= $_GET['p'];
$composition= $compositiondb->read($idcomposition);
$user= $userdb->read($composition->iduser);
$categorie= $categoriedb->read($composition->idcategorie);
$domaine= $domainedb->read($categorie->iddomaine);

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier une Composition
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=composition'">
			Liste des Compositions
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur la Composition
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/compositionController.php?action=update" enctype="multipart/form-data">
					<div class="form-group none">
					    <label for="idcomposition">ID</label>
					    <input type="text" name="idcomposition" class="form-control" id="idcomposition" placeholder="ID" required value="<?php echo $composition->idcomposition; ?>" />
					</div>


					<div class="form-group">
					    <label for="idcategorie">
					    	Sélectionnez une catégorie
					    </label>

					    <select name="idcategorie" id="idcategorie" class="form-control">
					    	<option value="<?php echo $categorie->idcategorie ?>" class="none" selected>
					    		<?php echo $categorie->intitule ?>
					    	</option>

					    	<?php 
					    	foreach ($categories as $categorie) {
					    	?>
					    	
					    	<option value="<?php echo $categorie->idcategorie ?>">
					    		<?php echo $categorie->intitule ?>
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="code">Code</label>
					    <input type="text" name="code" class="form-control" id="code" placeholder="Entrez le code" value="<?php echo $composition->code; ?>" />
					</div>

					<div class="form-group">
					    <label for="intitule">Intitulé</label>
					    <input type="text" name="intitule" class="form-control" id="intitule" placeholder="Intitulé" required value="<?php echo $composition->intitule; ?>" />
					</div>

					<div class="form-group">
					    <label for="description">Description</label>
					    <textarea name="description" class="form-control" id="description" placeholder="Donnez une description"><?php echo $composition->description; ?></textarea>
					</div>

					<div class="form-group">
					    <label for="duree">Durée (en Heure)</label>
					    <input type="number" name="duree" class="form-control" id="duree" placeholder="Durée (en Heure)" required value="<?php echo $composition->duree; ?>" />
					</div>

					<div class="form-group">
					    <label for="nbquestions">Nombre de questions</label>
					    <input type="number" name="nbquestions" class="form-control" id="nbquestions" placeholder="Nombre de questions" required value="<?php echo $composition->nbquestions; ?>" />
					</div>



					<div class="form-group">
					    <label for="etat">Etat</label>
					    <select name="etat" class="form-control" id="etat">
					    	<option value="<?php echo $composition->etat; ?>" class="none" selected>
					    		<?php echo $composition->etat; ?>
					    	</option>

					    	<option value="Activé">Activé</option>
					    	<option value="Désactivé">Désactivé</option>
					    </select>
					</div>



					<div class="form-group">
						<label for="image">
							Modifier l'image
						</label>

						<p class="text-center">
							<?php if($composition->image == '' || $composition->image == null) { ?>

			                <img src="img/avatar.png" alt="Image" class="img-responsive" />

			                <?php } else { ?>

			                <img src="<?php echo $res_composition.$composition->image ?>" alt="Image" class="img-responsive" />

			                <?php } ?>
						</p>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>

					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>