<?php

$users= $userdb->readAll();
$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();
$compositions= $compositiondb->readAll();

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Ajouter une Composition
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=composition'">
			Liste des Compositions
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur la composition
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/compositionController.php?action=create" enctype="multipart/form-data">
					<div class="form-group">
					    <label for="idcategorie">
					    	Sélectionnez une catégorie
					    </label>

					    <select name="idcategorie" id="idcategorie" class="form-control">
					    	<?php 
					    	foreach ($categories as $categorie) {
					    	?>
					    	
					    	<option value="<?php echo $categorie->idcategorie ?>">
					    		<?php echo $categorie->intitule ?>
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="code">Code</label>
					    <input type="text" name="code" class="form-control" id="code" placeholder="Entrez le code" />
					</div>

					<div class="form-group">
					    <label for="intitule">Intitulé</label>
					    <input type="text" name="intitule" class="form-control" id="intitule" placeholder="Intitulé" required />
					</div>

					<div class="form-group">
					    <label for="description">Description</label>
					    <textarea name="description" class="form-control" id="description" placeholder="Donnez une description"></textarea>
					</div>

					<div class="form-group">
					    <label for="duree">Durée (en Heure)</label>
					    <input type="number" name="duree" class="form-control" id="duree" placeholder="Durée (en Heure)" required value="1" />
					</div>

					<div class="form-group">
					    <label for="nbquestions">Nombre de questions</label>
					    <input type="number" name="nbquestions" class="form-control" id="nbquestions" placeholder="Nombre de questions" required value="5" />
					</div>

					<div class="form-group">
					    <label for="etat">Etat</label>
					    <select name="etat" class="form-control" id="etat">
					    	<option value="Activé">Activé</option>
					    	<option value="Désactivé">Désactivé</option>
					    </select>
					</div>

					<div class="form-group">
						<label for="image">
							Ajoutez une image
						</label>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>

					<button type="submit" class="btn btn-primary mr-2 float-right">
						Enregistrer
					</button>
				</form>
			</div>
		</div>
	</div>
</div>