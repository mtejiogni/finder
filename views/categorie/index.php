<?php

$domaines= $domainedb->readAll();
$categories= $categoriedb->readAll();

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des catégories
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=categorie_create'">
			Ajouter une catégorie
		</button>
	</div>
</div>



<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des catégories
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-bordered">
			            <thead>
			            	<tr>
				                <th>#</th>
				                <th>Domaine</th>
				                <th>Intitulé</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($categories != null && sizeof($categories) != 0) {
								$i= 0;

								foreach($categories as $categorie) {
									$domaine= $domainedb->read($categorie->iddomaine);

									$update= 'app.php?view=categorie_update&p='. $categorie->idcategorie;
									$delete= 'controller/categorieController.php?action=delete&p='. $categorie->idcategorie;
									$i= $i+1;
							?>

			            	<tr class="element">
				                <td class="data"><?php echo $i; ?></td>
				                <td class="data"><?php echo $domaine->intitule; ?></td>
				                <td class="data"><?php echo $categorie->intitule; ?></td>

				                <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
			            	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>