<?php

$domaines= $domainedb->readAll();

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Ajouter une catégorie
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=categorie'">
			Liste des catégories
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur la catégorie
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/categorieController.php?action=create">

					<div class="form-group">
					    <label for="iddomaine">
					    	Sélectionnez un domaine
					    </label>

					    <select name="iddomaine" id="iddomaine" class="form-control">
					    	<?php 
					    	foreach ($domaines as $domaine) {
					    	?>
					    	
					    	<option value="<?php echo $domaine->iddomaine ?>">
					    		<?php echo $domaine->intitule ?>
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="intitule">Intitulé</label>
					    <input type="text" name="intitule" class="form-control" id="intitule" placeholder="Intitulé" required />
					</div>


					<button type="submit" class="btn btn-primary mr-2 float-right">
						Enregistrer
					</button>
				</form>
			</div>
		</div>
	</div>
</div>