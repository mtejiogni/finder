<?php

$idquestion= $_GET['p'];
$question= $questiondb->read($idquestion);

$propositions= $propositiondb->readIdquestion($idquestion);

?>



<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des Propositions
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='<?php echo 'app.php?view=proposition_create&p='.$idquestion ?>'">
			Ajouter une Proposition
		</button>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<p class="text-success">
					<b><u>Enoncé de la Question :</u></b>
					<br />
					<?php echo $question->enonce; ?>
				</p>

				<hr />

	    		<h4 class="card-title">
	    			Liste des Propositions
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-striped">
			            <thead>
			            	<tr>
				                <th>Enoncé</th>
				                <th>Image</th>
				                <th>Fichier</th>
				                <th>Réponse</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($propositions != null && sizeof($propositions) != 0) {
								$i= 0;

								foreach($propositions as $proposition) {
									
									$class_reponse= '';
									if($proposition->reponse == 'Oui') {
										$class_reponse= 'btn btn-success';
									}
									else if($proposition->reponse == 'Non') {
										$class_reponse= 'btn btn-danger';
									}

									$reponse= 'controller/propositionController.php?action=updateReponse&p='. $proposition->idproposition .'&reponse=';

									$update= 'app.php?view=proposition_update&p='. $proposition->idproposition;
									$delete= 'controller/propositionController.php?action=delete&p='. $proposition->idproposition;
									$i= $i+1;
							?>

							<tr class="element">
	                            <td class="data"><?php echo $proposition->enonce; ?></td>


	                            <td class="py-1">
	                            	<?php if($proposition->image != '' || $proposition->image != null) { ?>

					                <img src="<?php echo $res_proposition.$proposition->image ?>" alt="Image" />

					                <?php } ?>
	                            </td>




	                            <td>
	                            	<?php if($proposition->fichier != '' || $proposition->fichier != null) { ?>

									<a href="<?php echo $res_proposition.$proposition->fichier ?>" target="blank">
										<span class="fas fa-file fa-2x"></span>
									</a>

					                <?php } ?>
	                            </td>








	                            <td>
	                            	<div class="btn-group" role="group">
									    <button type="button" class="<?php echo $class_reponse; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    	<?php 
									    		if ($proposition->reponse == 'Oui') {
									    			echo '(Oui) <i class="fas fa-check"></i>';
									    		} 
									    		else {
									    			echo '(Non) <i class="fas fa-times"></i>';
									    		}
									    	?>
									    	<span class="caret"></span>
									    </button>

									    <ul class="dropdown-menu text-center">
									    	<li class="btn btn-success btn-xs" onclick="document.location.href='<?php echo $reponse . 'Oui' ?>'">
									    		<span>
									    			<i class="fas fa-check"></i>
									    		</span>
									    	</li>
									    	<li class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $reponse . 'Non' ?>'">
									    		<span>
									    			<i class="fas fa-times"></i>
									    		</span>
									    	</li>
									    </ul>
									</div>
	                            </td>
	                            <td class="data none"><?php echo $proposition->reponse; ?></td>











	                            <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
                        	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>