<?php

$idproposition= $_GET['p'];
$proposition= $propositiondb->read($idproposition);
$question= $questiondb->read($proposition->idquestion);

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier une Proposition
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='<?php echo 'app.php?view=proposition&p='.$question->idquestion ?>'">
			Liste des Propositions
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur la Proposition
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/propositionController.php?action=update" enctype="multipart/form-data">
					<div class="form-group none">
					    <label for="idproposition">ID</label>
					    <input type="text" name="idproposition" class="form-control" id="idproposition" placeholder="ID" required value="<?php echo $proposition->idproposition; ?>" />
					</div>


					<div class="form-group">
					    <label for="enonce">Enoncé</label>
					    <textarea name="enonce" class="form-control" id="enonce" placeholder="Enoncé de la proposition" required><?php echo $proposition->enonce; ?></textarea>
					</div>



					<div class="form-group">
					    <label for="reponse">
					    	Cette proposition est-elle la réponse à la question posée ?
					    </label>
					    <select name="reponse" class="form-control" id="reponse">
					    	<option value="<?php echo $proposition->reponse; ?>" class="none" selected>
					    		<?php echo $proposition->reponse; ?>
					    	</option>

					    	<option value="Oui">Oui</option>
					    	<option value="Non">Non</option>
					    </select>
					</div>






					<div class="form-group">
						<label for="image">
							Modifier l'image
						</label>

						<p class="text-center">
							<?php if($proposition->image != '' || $proposition->image != null) { ?>

			                <img src="<?php echo $res_proposition.$proposition->image ?>" alt="Image" class="img-responsive" />

			                <?php } ?>
						</p>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>




					<div class="form-group">
						<label for="fichier">
							Modifier le fichier
						</label>

						<p class="text-center">
							<?php if($proposition->fichier != '' || $proposition->fichier != null) { ?>

							<a href="<?php echo $res_proposition.$proposition->fichier ?>" target="blank">
								<span class="fas fa-file fa-2x"></span>
							</a>

			                <?php } ?>
						</p>


						<input type="file" id="fichier" name="fichier" class="image form-control file" data-browse-on-zone-click="true" />

						<script type="text/javascript">
							$('#fichier').fileinput();
						</script>
					</div>




					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>