<?php

$domaines= $domainedb->readAll();

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des domaines
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=domaine_create'">
			Ajouter un domaine
		</button>
	</div>
</div>



<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des domaines
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-bordered">
			            <thead>
			            	<tr>
				                <th>#</th>
				                <th>Intitulé</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($domaines != null && sizeof($domaines) != 0) {
								$i= 0;

								foreach($domaines as $domaine) {
									$update= 'app.php?view=domaine_update&p='. $domaine->iddomaine;
									$delete= 'controller/domaineController.php?action=delete&p='. $domaine->iddomaine;
									$i= $i+1;
							?>

			            	<tr class="element">
				                <td class="data"><?php echo $i; ?></td>
				                <td class="data"><?php echo $domaine->intitule; ?></td>

				                <td class="options text-center">
				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
			            	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>