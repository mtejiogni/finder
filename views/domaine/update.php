
<?php

$iddomaine= $_GET['p'];
$domaine= $domainedb->read($iddomaine);

?>

<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier un domaine
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=domaine'">
			Liste des domaines
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur le domaine
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/domaineController.php?action=update">
					<div class="form-group none">
					    <label for="iddomaine">ID DOMAINE</label>
					    <input type="text" name="iddomaine" class="form-control" id="iddomaine" placeholder="ID" required value="<?php echo $domaine->iddomaine; ?>" />
					</div>

					<div class="form-group">
					    <label for="intitule">Intitulé</label>
					    <input type="text" name="intitule" class="form-control" id="intitule" placeholder="Intitulé" required value="<?php echo $domaine->intitule; ?>" />
					</div>

					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>