<?php

$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();
$questions= $questiondb->readAll();

$idquestion= $_GET['p'];
$question= $questiondb->read($idquestion);
$categorie= $categoriedb->read($question->idcategorie);
$domaine= $domainedb->read($categorie->iddomaine);

?>


<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Modifier une Question
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=question'">
			Liste des Questions
		</button>
	</div>
</div>


<div class="row">
	<div class="col-12 grid-margin stretch-card">
		<div class="card">
			<div class="card-body">
				<h4 class="card-title">
					Informations sur la Question
				</h4>

				<form class="forms-sample" name="form" method="POST" action="controller/questionController.php?action=update" enctype="multipart/form-data">
					<div class="form-group none">
					    <label for="idquestion">ID</label>
					    <input type="text" name="idquestion" class="form-control" id="idquestion" placeholder="ID" required value="<?php echo $question->idquestion; ?>" />
					</div>


					<div class="form-group">
					    <label for="idcategorie">
					    	Sélectionnez une catégorie
					    </label>

					    <select name="idcategorie" id="idcategorie" class="form-control">
					    	<option value="<?php echo $categorie->idcategorie ?>" class="none" selected>
					    		<?php echo $categorie->intitule ?>
					    	</option>

					    	<?php 
					    	foreach ($categories as $categorie) {
					    	?>
					    	
					    	<option value="<?php echo $categorie->idcategorie ?>">
					    		<?php echo $categorie->intitule ?>
					    	</option>

					    	<?php 
					    	} 
					    	?>
					    </select>
					</div>


					<div class="form-group">
					    <label for="enonce">Enoncé</label>
					    <textarea name="enonce" class="form-control" id="enonce" placeholder="Enoncé de la question" required><?php echo $question->enonce; ?></textarea>
					</div>

					<div class="form-group">
					    <label for="nbpoints">Nombre de points (en entier)</label>
					    <input type="number" name="nbpoints" class="form-control" id="nbpoints" placeholder="Ex : 1" required value="<?php echo $question->nbpoints; ?>" />
					</div>


					<div class="form-group">
					    <label for="etat">Etat</label>
					    <select name="etat" class="form-control" id="etat">
					    	<option value="<?php echo $question->etat; ?>" class="none" selected>
					    		<?php echo $question->etat; ?>
					    	</option>

					    	<option value="Activé">Activé</option>
					    	<option value="Désactivé">Désactivé</option>
					    </select>
					</div>



					<div class="form-group">
					    <label for="niveau">Niveau de difficulté</label>
					    <select name="niveau" class="form-control" id="niveau">
					    	<option value="<?php echo $question->niveau; ?>" class="none" selected>
					    		<?php echo $question->niveau; ?>
					    	</option>

					    	<option value="Facile">Facile</option>
					    	<option value="Intermédiaire">Intermédiaire</option>
					    	<option value="Difficile">Difficile</option>
					    </select>
					</div>






					<div class="form-group">
						<label for="image">
							Modifier l'image
						</label>

						<p class="text-center">
							<?php if($question->image != '' || $question->image != null) { ?>

			                <img src="<?php echo $res_question.$question->image ?>" alt="Image" class="img-responsive" />

			                <?php } ?>
						</p>


						<input type="file" id="image" name="image" class="image form-control file" data-browse-on-zone-click="true" accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG, .gif, .GIF" />

						<script type="text/javascript">
							$('#image').fileinput({
							    theme: 'fas',
							    language: 'fr',
							    showUpload: false,
							    showCaption: true,
							    showDownload: true,
								showZoom: true,
								showDrag: true,
								maxImageWidth: 10,
								maxImageHeight: 10,
								resizeImage: true,
								maxFileSize: 10240, //en KO
							    /*uploadUrl: '#'*/
							});
						</script>
					</div>




					<div class="form-group">
						<label for="fichier">
							Modifier le fichier
						</label>

						<p class="text-center">
							<?php if($question->fichier != '' || $question->fichier != null) { ?>

							<a href="<?php echo $res_question.$question->fichier ?>" target="blank">
								<span class="fas fa-file fa-2x"></span>
							</a>

			                <?php } ?>
						</p>


						<input type="file" id="fichier" name="fichier" class="image form-control file" data-browse-on-zone-click="true" />

						<script type="text/javascript">
							$('#fichier').fileinput();
						</script>
					</div>




					<button type="submit" class="btn btn-primary mr-2 float-right">
						Modifier
					</button>
				</form>
			</div>
		</div>
	</div>
</div>