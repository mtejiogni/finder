<?php

$categories= $categoriedb->readAll();
$domaines= $domainedb->readAll();
$questions= $questiondb->readAll();

?>



<div class="page-header flex-wrap">
	<h3 class="mb-0">
		Gestion des Questions
		<span class="pl-0 h6 pl-sm-2 text-muted d-inline-block">
			#.
		</span>
	</h3>

	<div class="d-flex">
		<button type="button" class="btn btn-sm bg-white btn-icon-text border ml-3">
			<i class="mdi mdi-printer btn-icon-prepend"></i> Print
		</button>
		
		<button type="button" class="btn btn-sm ml-3 btn-success" onclick="document.location.href='app.php?view=question_create'">
			Ajouter une Question
		</button>
	</div>
</div>


<div class="row">
	<div class="col-lg-12 grid-margin stretch-card">
	    <div class="card">
	    	<div class="card-body">
	    		<h4 class="card-title">
	    			Liste des Questions
	    		</h4>

		        <div class="input-group">
                    <input type="text" class="form-control bordure-arrondi-gauche" placeholder="Recherche..." id="search" />
                    <div class="input-group-append">
                      <span class="input-group-text">
                        <i class="mdi mdi-magnify"></i>
                      </span>
                    </div>
                </div>

                
                <br />


		        <div class="table-responsive">
		        	<table class="table table-striped">
			            <thead>
			            	<tr>
			            		<th>Catégorie</th>
				                <th>Enoncé</th>
				                <th>Points</th>
				                <th>Image</th>
				                <th>Fichier</th>
				                <th>Etat</th>
				                <th>Niveau</th>
				                <th>Options</th>
			            	</tr>
			            </thead>

			            <tbody>

							<?php
							if($questions != null && sizeof($questions) != 0) {
								$i= 0;

								foreach($questions as $question) {
									$categorie= $categoriedb->read($question->idcategorie);
									$domaine= $domainedb->read($categorie->iddomaine);



									$class_etat= '';
									if($question->etat == 'Activé') {
										$class_etat= 'btn btn-success';
									}
									else if($question->etat == 'Désactivé') {
										$class_etat= 'btn btn-danger';
									}


									$class_niveau= '';
									if($question->niveau == 'Facile') {
										$class_niveau= 'btn btn-success';
									}
									else if($question->niveau == 'Intermédiaire') {
										$class_niveau= 'btn btn-warning';
									}
									else if($question->niveau == 'Difficile') {
										$class_niveau= 'btn btn-danger';
									}


									$etat= 'controller/questionController.php?action=updateEtat&p='. $question->idquestion .'&etat=';
									$niveau= 'controller/questionController.php?action=updateNiveau&p='. $question->idquestion .'&niveau=';
									$proposition= 'app.php?view=proposition&p='. $question->idquestion;
									$update= 'app.php?view=question_update&p='. $question->idquestion;
									$delete= 'controller/questionController.php?action=delete&p='. $question->idquestion;
									$i= $i+1;
							?>

							<tr class="element">
								<td class="data"><?php echo $categorie->intitule; ?></td>
	                            <td class="data"><?php echo substr($question->enonce, 0, 30).'...'; ?></td>
	                            <td class="data"><?php echo $question->nbpoints; ?></td>




	                            <td class="py-1">
	                            	<?php if($question->image != '' || $question->image != null) { ?>

					                <img src="<?php echo $res_question.$question->image ?>" alt="Image" />

					                <?php } ?>
	                            </td>




	                            <td>
	                            	<?php if($question->fichier != '' || $question->fichier != null) { ?>

									<a href="<?php echo $res_question.$question->fichier ?>" target="blank">
										<span class="fas fa-file fa-2x"></span>
									</a>

					                <?php } ?>
	                            </td>



	                            <td>
	                            	<div class="btn-group" role="group">
									    <button type="button" class="<?php echo $class_etat; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    	<?php echo $question->etat; ?>
									    	<span class="caret"></span>
									    </button>

									    <ul class="dropdown-menu text-center">
									    	<li class="btn btn-success btn-xs" onclick="document.location.href='<?php echo $etat . 'Activé' ?>'">
									    		<span>
									    			Activé
									    		</span>
									    	</li>
									    	<li class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $etat . 'Désactivé' ?>'">
									    		<span>
									    			Désactivé
									    		</span>
									    	</li>
									    </ul>
									</div>
	                            </td>
	                            <td class="data none"><?php echo $question->etat; ?></td>



	                            <td>
	                            	<div class="btn-group" role="group">
									    <button type="button" class="<?php echo $class_niveau; ?> dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									    	<?php echo $question->niveau; ?>
									    	<span class="caret"></span>
									    </button>

									    <ul class="dropdown-menu">
									    	<li onclick="document.location.href='<?php echo $niveau . 'Facile' ?>'">
									    		<div class="btn btn-success btn-xs" style="width:100%">
									    			Facile
									    		</div>
									    	</li>
									    	<li onclick="document.location.href='<?php echo $niveau . 'Intermédiaire' ?>'">
									    		<div class="btn btn-warning btn-xs" style="width:100%">
									    			Intermédiaire
									    		</div>
									    	</li>
									    	<li onclick="document.location.href='<?php echo $niveau . 'Difficile' ?>'">
									    		<div class="btn btn-danger btn-xs" style="width:100%">
									    			Difficile
									    		</div>
									    	</li>
									    </ul>
									</div>
	                            </td>
	                            <td class="data none"><?php echo $question->niveau; ?></td>




	                            





	                            <td class="options text-center">
	                            	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $proposition; ?>'" title="Ajouter une proposition">
				                		<i class="fas fa-check"></i>
				                	</button>

				                	<button class="btn btn-info btn-xs" onclick="document.location.href='<?php echo $update; ?>'">
				                		<i class="fas fa-pencil-alt"></i>
				                	</button>
				                	
									<button class="btn btn-danger btn-xs" onclick="document.location.href='<?php echo $delete; ?>'">
				                		<i class="fa fa-trash"></i>
				                	</button>
								</td>
                        	</tr>

							<?php
								}
							}
							?>

			            </tbody>
		        	</table>
		        </div>
	      	</div>
	    </div>
	</div>
</div>